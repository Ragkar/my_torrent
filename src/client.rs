use crate::{
    command::{
        ClientCommand,
        PeerCommand,
    },
    peer::{
        DataPackage,
        DataRequest,
        Peer,
    },
    throttle::Throttle,
    torrent::{
        TorrentInfo,
        Torrent,
    },
    tracker::TrackerEvent,
};

use tokio::{
    sync::mpsc,
    time::timeout,
};

use std::{
    collections::{
        HashMap,
        VecDeque,
    },
    io::Result,
    net::{
        IpAddr,
        Ipv4Addr,
        SocketAddr,
    },
    sync::{
        Arc,
        Mutex,
    },
    time::Duration,
};

pub struct PeerInfo {
    tx: mpsc::Sender<PeerCommand>,
    //fut: JoinHandle<()>,
    is_block_requested: Vec<Vec<bool>>,
    is_piece_downloaded: Vec<bool>,
}

impl PeerInfo {

    pub fn new(tx: mpsc::Sender<PeerCommand>,
            //fut: JoinHandle<()>,
            info: Arc<Mutex<TorrentInfo>>) -> PeerInfo {
        let torrent_info = info.lock().unwrap();

        let pieces_count = torrent_info.pieces_count;
        let mut is_block_requested: Vec<Vec<bool>> = Vec::with_capacity(pieces_count);
        for i in 0..pieces_count {
            let blocks_count = torrent_info.get_block_count(i);
            is_block_requested.push(vec![false; blocks_count]);
        }

        return PeerInfo {
            tx: tx,
            //fut: fut,
            is_block_requested: is_block_requested,
            is_piece_downloaded: vec![false; pieces_count]
        }
    }

    pub fn block_requested(&self) -> bool {
        for v in self.is_block_requested.iter() {
            for requested in v.iter() {
                if *requested {
                    return true;
                }
            }
        }
        return false;
        /*
        self.is_block_requested.iter()
            .fold(0 as usize, |acc, x|
                acc + x.iter().fold(0, |acc, &y| if y { 1 } else { 0 })
            )
        */
    }
}

pub struct Client {
    port: usize,
    info: Arc<Mutex<TorrentInfo>>,
    torrent: Torrent,
    id: String,
    // TODO: use random
    random: usize,

    tx: mpsc::Sender<ClientCommand>,
    rx: mpsc::Receiver<ClientCommand>,

    peers: HashMap<String, PeerInfo>,
    peers_runner: Vec<Peer>,
    seeders: Vec<String>,
    leechers: Vec<String>,

    incoming_block: VecDeque<DataPackage>,
    outgoing_block: VecDeque<DataRequest>,

    max_leechers: usize,
    max_seeders: usize,
    max_upload_bytes: usize,
    max_download_bytes: usize,

    is_stopping: bool,
    is_process_peers: bool,
    is_process_uploads: bool,
    is_process_downloads: bool,

    peer_timeout: Duration,
}

impl Client {
    pub async fn new(port: usize, torrent_path: String, download_path: String) -> Result<Client> {
        info!("Create client.");
        // TODO: random ID
        let id = "00110022113322443369";

        let (tx, rx) = mpsc::channel::<ClientCommand>(1000);
        let torrent = match Torrent::from_file(&torrent_path, &download_path, tx.clone()).await {
            Ok(x) => x,
            Err(e) => return Err(e),
        };

        Ok(Client {
            port: port,
            info: torrent.info.clone(),
            torrent: torrent,
            id: id.to_string(),
            random: 0,

            tx: tx,
            rx: rx,

            peers: HashMap::new(),
            peers_runner: Vec::new(),
            seeders: Vec::new(),
            leechers: Vec::new(),

            incoming_block: VecDeque::new(),
            outgoing_block: VecDeque::new(),

            max_leechers: 5,
            max_seeders: 5,
            max_upload_bytes: 16384,
            max_download_bytes: 100 * 1024 * 1024,

            is_stopping: false,
            is_process_peers: false,
            is_process_uploads: false,
            is_process_downloads: false,

            peer_timeout: Duration::from_secs(30),
        })
    }

    pub async fn start(&mut self) -> () {
        info!("Starting client");
        self.is_stopping = false;
        self.run().await;
    }

    pub async fn stop(&mut self) -> () {
        info!("Stopping client");
        self.is_stopping = true;
        let port = self.port.to_string();
        self.torrent.update_trackers(
            TrackerEvent::Started,
            self.id.as_str(),
            port.as_str()).await;

        let futs = self.peers.iter().map(|(_, p)| p.tx.send(PeerCommand::Disconnect));
        futures::future::join_all(futs).await;
    }

    pub async fn run(&mut self) {
        let port = self.port.to_string();

        self.torrent.update_trackers(
            TrackerEvent::Started,
            self.id.as_str(),
            port.as_str()).await;

        while !self.is_stopping {
            info!("Client::loop.");

            let duration = Duration::from_millis(500);
            while let ret = timeout(duration, self.rx.recv()).await {
                match ret {
                    Ok(Some(cmd)) => {
                        //info!("Client: run: handle peer command ({:?}).", cmd);
                        self.handle_command(cmd).await;
                    }
                    Ok(None) => break,
                    Err(e) => {
                        error!("Client: loop recv: {}.", e);
                        /*
                        self.disconnect(true);
                        */
                        break;
                    }
                }
            }
            /*
            let fut_peers = timeout(Duration::from_millis(500), self.rx.recv());
            let fut_ctrlc = timeout(Duration::from_millis(500), tokio::signal::ctrl_c());

            let (ret_peers, ret_ctrlc) = tokio::join!(fut_peers, fut_ctrlc);

            tokio::select! {
                /*
                _ = fut_trackers => {
                    //info!("Client: run: trackers.")
                },
                */
                Ok(_) = fut_ctrlc => {
                    info!("Client: run: ctrlc.");
                    self.stop().await;
                },
                Ok(Some(command)) = fut_peers => {
                    info!("Client: run: handle peer command ({:?}).", command);
                    self.handle_command(command).await;
                }
            }
            */
            self.process_downloads().await;

            //let down_fut = self.process_downloads().await;
            //let up_fut = self.process_uploads().await;
            //futures::future::join_all(vec![down_fut, up_fut]).await;

            info!("Client: loop: peers.size() == {}", self.peers.len());
            let fut_peer_vec = self.peers_runner
                .iter_mut()
                .map(|x| timeout(Duration::from_secs(5), x.runner()));
            futures::future::join_all(fut_peer_vec).await;
        }
    }

    pub async fn handle_command(&mut self, command: ClientCommand) {
        match command {
            ClientCommand::BlockCancel { id, piece, begin, length, }
                => self.handle_block_cancelled(id, piece, begin, length).await,
            ClientCommand::BlockReceived { id, piece, block, data, }
                => self.handle_block_received(id, piece, block, data).await,
            ClientCommand::BlockRequest { id, index, begin, length, }
                => self.handle_block_requested(id, index, begin, length).await,
            ClientCommand::Disconnected { id, }
                => self.handle_peer_disconnected(id),
            ClientCommand::ListUpdate { sockets, }
                => self.handle_peer_list_updated(sockets).await,
            ClientCommand::PieceVerify { piece, }
                => self.handle_piece_verified(piece).await,
            ClientCommand::SetSeeder { id, }
                => self.handle_set_seeder(id).await,
            ClientCommand::StateChanged { id, pieces_downloaded, }
                => self.handle_peer_state_changed(id, pieces_downloaded).await,
        }
    }

    fn local_ip() -> IpAddr {
        let addr: Ipv4Addr = get_local_ip::network()
            .unwrap()
            .ip
            .parse()
            .expect("failed to parse own IpAddr");
        IpAddr::V4(addr)
    }

    /*
    async fn handle_peer_list_updated(&mut self, peers: Vec<SocketAddr>) {
        //let local = Client::local_ip();
        let local = "11002299338844775566";

        if peers.len() > 0 {
            for i in 0..peers.len() {
                let opt = Client::connect_peer(local.to_string(), peers[i], self.info.clone(), self.tx.clone()).await;
                if let Some((key, info)) = opt {
                    self.peers.insert(key, info);
                    break;
                }
            }
        }

        /*
        let futs = peers.iter()
            .filter(|x| x.ip() != local)
            .map(|&x| Client::connect_peer(local.to_string(), x, self.info.clone(), self.tx.clone()));

        let tuples = futures::future::join_all(futs)
            .await
            .into_iter()
            .filter(|x| x.is_some())
            .map(|x| x.unwrap());

        for (key, info) in tuples {
            self.peers.insert(key, info);
        }
        */
    }
    */

    async fn handle_peer_list_updated(&mut self, sockets: Vec<SocketAddr>) {
        let local = "11002299338844775566";
        let ip = Client::local_ip();

        let mut futs = Vec::new();
        let mut txs = Vec::new();
        for socket in sockets.iter() {
            if socket.ip() != ip {
                let (tx, rx) = mpsc::channel::<PeerCommand>(500);
                txs.push(tx);
                let p = Peer::new(self.info.clone(), local.to_string(), *socket, self.tx.clone(), rx);
                futs.push(p);
            }
        }

        let mut some_peers = futures::future::join_all(futs).await;
        info!("Client: socket connected.");

        let mut idx = 0;
        while idx < some_peers.len() {
            if some_peers[idx].is_some() {
                info!("Client: add peer.");
                let p = some_peers.remove(idx).unwrap();
                self.peers.insert(p.key.clone(), PeerInfo::new(txs.remove(idx), self.info.clone()));
                self.peers_runner.push(p);
            } else {
                idx += 1;
            }
        }
        let fut_connect = self.peers_runner.iter_mut().map(|x| x.connect());
        futures::future::join_all(fut_connect).await;
    }

    /*
    async fn connect_peer(local: String,
            socket: SocketAddr,
            info: Arc<Mutex<TorrentInfo>>,
            client_tx: mpsc::Sender<ClientCommand>) -> Option<(String, PeerInfo)> {

        let (tx, rx) = mpsc::channel::<PeerCommand>(200);
        let opt_peer = Peer::new(info.clone(), local, socket, client_tx, rx).await;
        if opt_peer.is_none() {
            return None;
        }

        let mut peer = opt_peer.unwrap();
        let key = peer.key.clone();

        match peer.connect().await {
            Ok(_) => {
                let fut = tokio::task::spawn(async move {
                    peer.run().await;
                });
                let peer_info = PeerInfo::new(tx, /*fut,*/ info);
                Some((key, peer_info))
            },
            Err(_) => None,
        }
    }
    */

    pub fn handle_peer_disconnected(&mut self, key: String) {
        info!("-> handle_peer_disconnected.");
        self.peers.remove(&key);
        if let Some(index) = self.seeders.iter().position(|x| *x == key) {
            self.seeders.remove(index);
        }
        if let Some(index) = self.leechers.iter().position(|x| *x == key) {
            self.leechers.remove(index);
        }
    }

    async fn handle_peer_state_changed(&mut self, id: String, pieces_downloaded: Option<Vec<usize>>) {
        info!("Client: handle_peer_state_changed.");
        if let Some(pieces_index) = pieces_downloaded {
            info!("Client: add pieces {:?}.", pieces_index);
            let peer_info = self.peers.get_mut(id.as_str()).unwrap();
            for piece in pieces_index {
                peer_info.is_piece_downloaded[piece] = true;
            }
        }
        self.process_peers().await;
    }

    async fn handle_piece_verified(&mut self, index: usize) {
        info!("-> handle_piece_verified.");
        self.process_peers().await;
        let fut = self.peers.iter().map(|(k, p)| p.tx.send(
                PeerCommand::PieceVerified{ index: index }
            ));
        futures::future::join_all(fut).await;
        /*
        // TODO: async
        for (key, peer_info) in self.peers.iter() {
            if let Err(_) = peer_info.tx.send(PeerCommand::PieceVerified{
                index: index
            }).await {
                info!("Client: handle_piece_verified: send failed for {}.", key);
            }
        }
        */
    }

    async fn handle_set_seeder(&mut self, key: String) {
        info!("-> handle_set_seeder.");
        match self.seeders.iter().position(|x| *x == key) {
            //Some(p) => panic!("Seeder {} already exist at position {} !", key, p),
            Some(p) => info!("Seeder {} already exist at position {} !", key, p),
            None => self.seeders.push(key),
        }
    }

    async fn process_peers(&mut self) {
        info!("Client: process peer.");
        let info = self.info.lock().unwrap();
        let cmd = PeerCommand::Process {
            is_completed: info.is_completed(),
            is_started: info.is_started(),
            leechers_ok: Arc::new(Mutex::new((self.max_leechers as i8) - (self.leechers.len() as i8))),
            seeders_ok : Arc::new(Mutex::new((self.max_seeders as i8) - (self.seeders.len() as i8))),
            peer_timeout: self.peer_timeout.clone(),
        };
        drop(info);

        // TODO: async ?
        let seeds = &self.seeders;
        self.peers.iter_mut()
            .filter(|(k, _)| None == seeds.iter().position(|x| x == *k))
            .for_each(|(k, p)| match p.tx.try_send(cmd.clone()) {
                Err(e) => info!("Client: process_peers: Err({}).", e),
                Ok(_) => info!("Client: process_peers Ok({})", k),
            });

        /*
        for (key, peer_info) in &mut self.peers.iter_mut() {
            // TODO: bad way, bad complexity
            if None == self.seeders.iter().position(|x| x == key) {
                info!("Client: process peer: send cmd.");
                /*
                // TODO: WORKING HERE !!!!!
                */

                if let Err(_) = peer_info.tx.send(cmd.clone()).await {
                    info!("Client: process_peers: send failed for {}", key);
                }
            }
        }
        */
        info!("Client: process peer. END");
    }

    async fn handle_block_requested(&mut self, id: String, piece: usize, begin: usize, length: usize) {
        info!("-> handle_block_requested.");
        self.outgoing_block.push_back(DataRequest {
            peer_id: id,
            piece: piece,
            begin: begin,
            length: length,
            is_cancelled: false,
        });
        self.process_uploads().await;
    }

    async fn handle_block_cancelled(&mut self, id: String, piece: usize, begin: usize, length: usize) {
        info!("-> handle_block_cancelled.");
        for block in self.outgoing_block.iter_mut() {
            if id == block.peer_id
                    && piece == block.piece
                    && begin == block.begin
                    && length == block.length {
                block.is_cancelled = true;
            }
        }
        self.process_uploads().await;
    }

    async fn process_uploads(&mut self) {
        info!("Client: process upload.");
        let mut throttle = Throttle::new(Duration::from_secs(1), self.max_upload_bytes);

        while !self.outgoing_block.is_empty() {
            let block = self.outgoing_block.pop_front().unwrap();

            if block.is_cancelled || self.info.lock().unwrap().is_piece_verified[block.piece] {
                continue;
            }

            // possible loop in torrent
            match self.torrent.read_block(block.piece, block.begin, block.length) {
                Ok(vec) => {
                    if let Err(_) = self.peers
                        .get(&block.peer_id).unwrap()
                        .tx.send(PeerCommand::SendPiece{
                            piece: block.piece,
                            begin: block.begin,
                            data: vec,
                        }).await {
                            info!("Client: process_uploads: send failed for {}.", block.peer_id);
                        }
                    //block.peer.send_piece(block.piece, block.begin, vec).await.unwrap(),
                }
                Err(_) => continue,
            }

            self.torrent.uploaded += block.length;
            throttle.add_bytes(block.length);
            throttle.check_and_wait().await;
        }
    }

    async fn handle_block_received(&mut self,
            id: String,
            piece: usize,
            block: usize,
            data: Vec<u8>) {
        info!("Client: handle_block_received.");
        let block_size = self.info.lock().unwrap().block_size;
        self.peers.get_mut(id.as_str())
            .unwrap()
            .is_block_requested[piece][block] = false;

        // TODO: async ?
        for (key, peer_info) in self.peers.iter_mut() {

            if peer_info.is_block_requested[piece][block] {
                if let Err(_) = peer_info.tx.send(PeerCommand::SendCancel {
                    piece: piece,
                    begin: block * block_size,
                    length: block_size,
                }).await {
                    info!("Client: handle_block_received: send failed for {}", key);
                }
                //peer.send_cancel(piece, block * block_size, block_size).await.unwrap();
                // TODO: set non requested ?
            }
        }

        self.incoming_block.push_back(DataPackage {
            peer_id: id,
            piece: piece,
            block: block,
            data: data,
        });
        self.process_downloads().await;
    }

    async fn process_downloads(&mut self) {
        info!("Client: process download.");
        // TODO: async + error !
        while let Some(block) = self.incoming_block.pop_front() {
            info!("Client: process_downloads: write block !");
            self.torrent.write_block(block.piece, block.block, block.data).await.unwrap();
        }

        if self.info.lock().unwrap().is_completed() {
            // Stop all ?
            info!("Client: process_downloads: is completed.");
            return;
        }

        let mut throttle = Throttle::new(Duration::from_secs(1), self.max_download_bytes);
        let mut usable_peer = self.seeders.iter()
            .filter(|key| match self.peers.get(*key) {
                Some(peer) => !peer.block_requested(),
                None => false,
            }).collect::<Vec<&String>>();
        for piece in self.get_ranked_pieces() {
            if usable_peer.len() == 0 {
                break;
            }
            // Skip if piece already acquired
            if self.info.lock().unwrap().is_piece_verified[piece] {
                info!("-> piece is verified.");
                continue;
            }
            let count = self.info.lock().unwrap().get_block_count(piece);

            for block in 0..count {
                if usable_peer.len() == 0 {
                    break;
                }

                // Skip if block already acquired
                if self.info.lock().unwrap().is_block_acquired[piece][block] {
                    info!("-> block is acquired.");
                    continue;
                }

                // Skip if block already request
                let mut peer_on_it = false;
                for (_, p) in self.peers.iter() {
                    if p.is_block_requested[piece][block] {
                        info!("-> block is requested.");
                        continue;
                    }
                }
                /*
                let mut peers_on_it = self.peers.iter()
                    .fold(0 as usize, |acc, (_, p)|
                        if p.is_block_requested[piece][block] { acc + 1 } else { acc }
                    );
                */
                let mut idx = 0;
                while idx < usable_peer.len() {
                    let key = usable_peer[idx];
                    idx += 1;

                    // Only one peer per block
                    if peer_on_it {
                        //info!("-> peer already on it.");
                        break;
                    }

                    let some_peer = self.peers.get_mut(key);
                    if some_peer.is_none() {
                        //info!("-> peer not found.");
                        continue;
                    }
                    let peer = some_peer.unwrap();

                    // Change block if piece is already downloaded
                    if !peer.is_piece_downloaded[piece] {
                        //info!("-> piece downloaded.");
                        break;
                    }

                    // Only one request at a time
                    if peer.block_requested() {
                        //info!("-> peer already in use.");
                        continue;
                    }

                    info!("Client: process_download: send request Peer({})[{}][{}]", key, piece, block);
                    let size = self.info.lock().unwrap().get_block_size(piece, block);
                    let block_size = self.info.lock().unwrap().block_size;

                    if let Err(_) = peer.tx.try_send(PeerCommand::SendRequest {
                        index: piece,
                        begin: block * block_size,
                        length: size
                    }) {
                        info!("Client: process_downloads: send failed for {}", key);
                    } else {
                        peer_on_it = true;
                        peer.is_block_requested[piece][block] = true;

                        idx -= 1;
                        usable_peer.remove(idx);

                        throttle.add_bytes(size);
                    }

                    throttle.check_and_wait().await;
                }
            }
        }
        info!("Client: process download: END.");
    }


    fn get_ranked_pieces(&self) -> Vec<usize> {
        let count = self.info.lock().unwrap().pieces_count;
        let mut pieces: Vec<usize> = (0..count).collect();
        let scores: Vec<f32>= pieces.iter()
            .map(|x| self.get_piece_score(x))
            .collect();
        pieces.sort_by(|a, b| {
            scores[*a].partial_cmp(&scores[b.clone()]).unwrap()
        });
        return pieces;
    }

    fn get_piece_score(&self, piece: &usize) -> f32 {
        let progress = self.get_piece_progress(piece.clone());
        if progress == 1.0 {
            return 0.0;
        }

        let rarity = self.get_piece_rarity(piece);

        return progress + rarity;
    }

    fn get_piece_progress(&self, piece: usize) -> f32 {
        let acquired = &self.info.lock().unwrap()
            .is_block_acquired[piece];
        let sum = acquired.iter()
            .fold(0.0, |acc, x| acc + if *x { 1.0 } else { 0.0 });
        let len = acquired.len() as f32;
        return sum / len;
    }

    fn get_piece_rarity(&self, piece: &usize) -> f32 {
        if self.peers.len() < 1 {
            return 0.0;
        }

        let sum = self.peers.iter()
            .fold(0.0, |acc, (_, p)| {
                if p.is_piece_downloaded[piece.clone()] {
                    acc
                } else {
                    acc + 1.0
                }
            });
        let len = self.peers.len() as f32;
        return sum / len;
    }
}
