use crate::be_encoding::BEObject;
use crate::command::ClientCommand;
use crate::tracker::{
    Tracker,
    TrackerEvent,
};

use crypto::sha1::Sha1;
use crypto::digest::Digest;
use futures::future;
use tokio::sync::mpsc::Sender;

use std::collections::{
    HashMap,
};
use std::cmp;
use std::fs::File;
use std::fs;
use std::io::{
    Error,
    ErrorKind,
    Result,
    SeekFrom,
    prelude::*,
};
use std::os::unix::prelude::FileExt;
use std::path::Path;
use std::net::SocketAddr;
use std::sync::{
    Arc,
    Mutex,
    RwLock,
};
use std::str;

pub struct FileItem {
    path: String,
    size: usize,
    offset: usize,
}

pub struct TorrentInfo {
    pub is_piece_verified: Vec<bool>,
    pub is_block_acquired: Vec<Vec<bool>>,
    pub info_hash: Vec<u8>,

    pub pieces_count: usize,
    pub block_size: usize,
    pub piece_size: usize,
    pub total_size: usize,
}

pub struct Torrent {
    pub info: Arc<Mutex<TorrentInfo>>,
    tx: Sender<ClientCommand>,

    is_private: Option<bool>,
    files: Vec<RwLock<FileItem>>,
    download_directory: String,
    trackers: Vec<Tracker>,
    comment: Option<String>,
    created_by: Option<String>,
    //creation_date: Instant,
    //encoding: ..,
    pieces_hashes: Vec<Vec<u8>>,

    pub name: String,
    pub uploaded: usize,
    pub info_hash_str: String,
}

impl TorrentInfo {

    pub fn verified_piece_count(&self) -> usize {
        self.is_piece_verified.iter()
            .map(|x| if *x { 1 } else { 0 } )
            .sum()
    }

    pub fn is_piece_verified(&self, i: usize) -> bool {
        self.is_piece_verified[i]
    }

    pub fn is_completed(&self) -> bool {
        self.verified_piece_count() == self.pieces_count
    }

    pub fn is_started(&self) -> bool {
        self.verified_piece_count() > 0
    }

    pub fn downloaded(&self) -> usize {
        self.verified_piece_count() * self.piece_size
    }

    pub fn get_block_count(&self, piece: usize) -> usize {
        self.get_piece_size(piece) / self.block_size
    }

    pub fn get_block_size(&self, piece: usize, block: usize) -> usize {
        if block == (self.get_block_count(piece) - 1) {
            let remainder = self.get_piece_size(piece) % self.block_size;
            if remainder != 0 {
                return remainder;
            }
        }

        self.block_size
    }

    pub fn get_piece_size(&self, piece: usize) -> usize {
        if piece == (self.pieces_count - 1) {
            let remainder = self.total_size % self.piece_size;
            if remainder != 0 {
                return remainder;
            }
        }

        self.piece_size
    }

    pub fn new(total_size: usize, piece_size: usize, block_size: usize) -> TorrentInfo {
        // TODO: +1 is necessary ?
        let piece_count = (total_size / piece_size) + 1;

        let mut is_piece_verified = Vec::with_capacity(piece_count );
        is_piece_verified.resize(piece_count , false);

        let mut info = TorrentInfo {
            is_piece_verified: is_piece_verified,
            is_block_acquired: Vec::with_capacity(piece_count),
            info_hash: Vec::new(),

            pieces_count: piece_count,
            block_size: block_size,
            piece_size: piece_size,
            total_size: total_size,
        };

        for i in 0..piece_count {
            let capacity = info.get_block_count(i);
            let mut v = Vec::with_capacity(capacity);
            v.resize(capacity, false);
            info.is_block_acquired.push(v);
        }

        return info;
    }

}

impl Torrent {

    fn file_directory(&self) -> String {
        if self.files.len() > 1 {
            format!("{}/", self.name)
        } else {
            "".to_string()
        }
    }

    /*-- Function to manage memory ------------------------------------------*/

    fn total_size(&self) -> usize {
        self.files.iter()
            .map(|f| f.read().unwrap().size)
            .sum()
    }

    pub fn left(&self) -> usize {
        self.total_size() - self.info.lock().unwrap().downloaded()
    }

    pub fn convert_to_url(vec: &[u8]) -> String {
        return vec[0..20]
            .iter()
            .map(|x| match x {
                48 ..= 57 | 65 ..= 90 | 97 ..= 122 | 45 | 95 | 46 | 126 => (*x as char).to_string(),
                _ => format!("%{:02X}", x),
            })
            .collect::<String>();
    }

    /*-- Constructor --------------------------------------------------------*/

    pub async fn new(name: &str,
               location: &str,
               files: Vec<FileItem>,
               trackers: Vec<String>,
               piece_size: usize,
               piece_hashes: Option<Vec<u8>>,
               block_size: usize,
               is_private: Option<bool>,
               comment: Option<String>,
               created_by: Option<String>,
               tx: Sender<ClientCommand>) -> Torrent {

        let total_size: usize = files.iter().map(|f| f.size).sum();
        // TODO: +1 is necessary ?
        let piece_count = (total_size / piece_size) + 1;

        let info = TorrentInfo::new(total_size, piece_size, block_size);

        let lockable_files = files.into_iter()
            .map(|f| RwLock::new(f))
            .collect();

        // TODO: add hook
        let hooked_trackers: Vec<Tracker> = trackers.into_iter()
            .map(|s| Tracker::new(&s))
            .collect();

        let mut pieces_hashes = Vec::with_capacity(piece_count );
        pieces_hashes.resize(piece_count , Vec::with_capacity(20));

        let mut torrent = Torrent {
            info: Arc::new(Mutex::new(info)),
            tx: tx,

            name: name.to_string(),
            files: lockable_files,
            download_directory: location.to_string(),
            trackers: hooked_trackers,
            comment: comment.map(|x| x.to_string()),
            created_by: created_by.map(|x| x.to_string()),
            //creation_date: ...,
            //encoding: ..,
            is_private: is_private,
            uploaded: 0,
            pieces_hashes: pieces_hashes,

            info_hash_str: String::new(),
        };

        if let Some(ph) = piece_hashes {
            for i in 0..piece_count {
                let slice = &ph[(i * 20)..((i + 1) * 20)];
                let mut v = Vec::with_capacity(20);
                v.resize(20, 0);
                v.clone_from_slice(slice);
                torrent.pieces_hashes[i] = v;
            }
        } else {
            for i in 0..piece_count {
                torrent.pieces_hashes[i] = torrent.get_hash(i).unwrap();
            }
        }

        match torrent.info_to_beobject().to_buffer() {
            Ok(buffer) => {
                let mut sha1 = Sha1::new();
                sha1.input(&buffer);

                let len = sha1.output_bytes();
                let mut info_hash = vec![0; len];
                sha1.result(&mut info_hash);

                torrent.info_hash_str = Torrent::convert_to_url(&info_hash[0..20]);
                torrent.info.lock().unwrap().info_hash = info_hash;
            },
            Err(_) => panic!("Torrent::new() fail to fill info_hash.")
        };

        for i in 0..piece_count {
            torrent.verify(i).await;
        }

        return torrent;
    }

    /*-- From BEncoding -----------------------------------------------------*/

    pub async fn from_beobject(beobject: BEObject, name: &str, download_path: &str, tx: Sender<ClientCommand>)
            -> Torrent {
        let mut map: HashMap<String, BEObject> = match beobject {
            BEObject::Dict { val: vd } => vd,
            _ => panic!("Torrent::from_beobject() fail to read beobject."),
        };

        let mut key = "announce";
        let trackers = match map.remove(key) {
            Some(BEObject::List { val: vl }) => vl.into_iter().map(|e| match e {
                BEObject::Str { val: vs } => String::from_utf8(vs).unwrap(),
                _ => panic!("Torrent::from_beobject() invalid \"{}\" struct.", key),
            }).collect::<Vec<String>>(),
            Some(BEObject::Str { val: vs }) => [String::from_utf8(vs).unwrap()].to_vec(),
            Some(x) => panic!("Torrent::from_beobject() \"{}\" is {}.", key, x),
            None => panic!("Torrent::from_beobject() \"{}\" is None.", key),
        };

        key = "created_by";
        let created_by = match map.remove(key) {
            Some(BEObject::Str { val: vs }) => Some(String::from_utf8(vs).unwrap()),
            _ => None,
        };

        key = "comment";
        let comment = match map.remove(key) {
            Some(BEObject::Str { val: vs }) => Some(String::from_utf8(vs).unwrap()),
            _ => None,
        };

        key = "info";
        let mut info = match map.remove(key) {
            Some(BEObject::Dict { val: vd }) => vd,
            _ => panic!("Torrent::from_beobject() fail to retrieve \"{}\".", key),
        };

        key = "piece length";
        let piece_size = match info.remove(key) {
            Some(BEObject::Int { val: vi }) => vi as usize,
            _ => panic!("Torrent::from_beobject() fail to retrieve \"{}\".", key),
        };

        key = "pieces";
        let pieces_hashes = match info.remove(key) {
            Some(BEObject::Str { val: vs }) => vs,
            _ => panic!("Torrent::from_beobject() fail to retrieve \"{}\".", key),
        };

        key = "private";
        let is_private = match info.remove(key) {
            Some(BEObject::Int { val: vi }) => Some(if vi == 1 { true } else { false }),
            None => None,
            _ => panic!("Torrent::from_beobject() fail to retrieve \"{}\".", key),
        };

        let files = match Torrent::files_from(&mut info) {
            Ok(f) => f,
            _ => panic!("Torrent::from_beobject() fail to retrieve FileItem."),
        };

        let torrent = Torrent::new(name,
            download_path,
            files,
            trackers,
            piece_size,
            Some(pieces_hashes),
            16384,
            is_private,
            comment,
            created_by,
            tx,
        ).await;
        return torrent;
    }

    fn files_from(info: &mut HashMap<String, BEObject>) -> Result<Vec<FileItem>> {
        let error = Err(Error::new(ErrorKind::InvalidInput, "Invalid input."));
        let mut files = Vec::new();

        if info.contains_key("name") && info.contains_key("length") {
            // Case of a singel file
            let p = match info.remove("name") {
                Some(BEObject::Str { val: vs }) => String::from_utf8(vs).unwrap(),
                _ => return error,
            };
            let s = match info.remove("length") {
                 Some(BEObject::Int { val: vi }) => vi as usize,
                _ => return error,
            };
            files.push(FileItem { path: p, size: s, offset: 0, });
        } else if info.contains_key("files") {
            // Case of multiple files
            let list = match info.remove("files") {
                Some(BEObject::List { val: vl }) => vl,
                _ => return error,
            };
            let mut offset = 0;
            for e in list {
                let mut d = match e {
                    BEObject::Dict { val: vd } => vd,
                    _ => return error,
                };
                let p = match d.remove("path") {
                    Some(BEObject::List { val: vl }) => vl.into_iter().map(|x| match x {
                            BEObject::Str { val: vs } => String::from_utf8(vs).unwrap(),
                            _ => "".to_string(),
                        }).collect::<Vec<String>>().join("/"),
                    _ => return error,
                };
                let s = match d["length"] {
                    BEObject::Int { val: vi } => vi as usize,
                    _ => return error,
                };
                files.push(FileItem { path: p, size: s, offset: offset, });
                offset += s;
            }
        } else {
            return error;
        }

        Ok(files)
    }

    /*-- To BEncoding -------------------------------------------------------*/

    fn to_beobject(self) -> BEObject {
        let mut map = HashMap::new();

        if self.trackers.len() == 1 {
            map.insert("announce".to_string(),
                BEObject::Str{ val: self.trackers[0].address.clone().into_bytes() });
        } else {
            let announce = self.trackers.iter()
                .map(|x| BEObject::Str { val: x.address.clone().into_bytes() })
                .collect();
            map.insert("announce".to_string(), BEObject::List { val: announce });
        }

        if let Some(comment) = self.comment.clone() {
            map.insert("comment".to_string(),
                BEObject::Str { val: comment.into_bytes() });
        }
        if let Some(created_by) = self.created_by.clone() {
            map.insert("created by".to_string(),
                BEObject::Str { val: created_by.into_bytes() });
        }
        // TODO: handle creation date
        map.insert("creation date".to_string(),
            BEObject::Str { val: "".to_string().into_bytes() });
        map.insert("info".to_string(), self.info_to_beobject());

        BEObject::Dict { val: map }
    }

    fn info_to_beobject(&self) -> BEObject {
        let mut map = HashMap::new();

        {
            let piece_size = self.info.lock().unwrap().piece_size;
            map.insert("piece length".to_string(),
                BEObject::Int { val: piece_size as u64 });
        }

        let mut pieces = Vec::new();
        for e in &self.pieces_hashes {
            pieces.extend_from_slice(e);
        }
        map.insert("pieces".to_string(), BEObject::Str { val: pieces });

        if let Some(is_private) = self.is_private {
            map.insert("private".to_string(),
                BEObject::Int { val: is_private as u64 });
        }

        if self.files.len() == 1 {
            let file = self.files[0].read().unwrap();
            map.insert("name".to_string(),
                BEObject::Str { val: file.path.clone().into_bytes() });
            map.insert("length".to_string(),
                BEObject::Int { val: file.size as u64});
        } else {
            let mut files = Vec::new();
            for f in &self.files {
                let file = f.read().unwrap();
                let mut file_map = HashMap::new();
                let list =  file.path.split('/')
                    .map(|x| BEObject::Str { val: x.to_string().into_bytes() })
                    .collect();
                file_map.insert("path".to_string(),
                    BEObject::List { val: list });
                file_map.insert("length".to_string(),
                    BEObject::Int { val: file.size as u64 });
                files.push(BEObject::Dict { val: file_map });
            }
            map.insert("files".to_string(), BEObject::List { val: files });
            map.insert("name".to_string(),
                BEObject::Str { val : self.file_directory().into_bytes() });
        }

        BEObject::Dict { val: map }
    }

    /*-- From/To File -------------------------------------------------------*/

    pub async fn from_file(file_path: &str, download_path: &str, tx: Sender<ClientCommand>) -> Result<Torrent> {
        let obj = BEObject::from_file(file_path)?;

        {
            let buff = obj.to_buffer().unwrap();
            fs::write("ouptup-bencode.torrent", buff).unwrap();
        }

        if let Some(oss) = Path::new(file_path).file_stem() {
            if let Some(name) = oss.to_str() {
                return Ok(Torrent::from_beobject(obj, name, download_path, tx).await);
            } else {
                return Err(Error::new(ErrorKind::InvalidInput, "Fail on OsStr.to_str()"));
            }
        } else {
            return Err(Error::new(ErrorKind::InvalidInput, "Fail on Path::file_stem()"));
        }
    }

    /*-- Create -------------------------------------------------------------*/

    fn get_files_rec(dir: &str, files: &mut Vec<FileItem>, offset: &mut usize)
        -> Result<()> {
        for entry in fs::read_dir(dir).unwrap() {
            let e = entry.unwrap();
            let filename = e.file_name();
            let path = e.path();

            if filename.to_str().unwrap().starts_with(".") {
                continue;
            } else if path.is_file() {
                let len = fs::metadata(path)?.len() as usize;
                files.push(FileItem {
                    path: filename.to_str().unwrap().to_string(),
                    size: len,
                    offset: *offset,
                });
                *offset += len;
            } else if path.is_dir() {
                Torrent::get_files_rec(path.to_str().unwrap(), files, offset)?;
            }
        }

        Ok(())
    }

    pub async fn create(path: &str,
                  trackers: Vec<String>,
                  piece_size: usize,
                  comment: Option<String>,
                  tx: Sender<ClientCommand>) -> Result<Torrent> {
        let mut name = "";
        let mut files = Vec::new();
        let p = Path::new(path);

        if p.is_file() {
            name = p.file_name().unwrap().to_str().unwrap();
            let len = fs::metadata(path)?.len();
            files.push(FileItem {
                path: name.to_string(),
                size: len as usize,
                offset: 0
            });
        } else if p.is_dir() {
            name = path;
            let dir = format!("{}/", path);
            let mut offset = 0;
            Torrent::get_files_rec(&dir, &mut files, &mut offset)?;
        } else {
            return Err(Error::new(ErrorKind::InvalidInput, "Path does not exist."));
        }

        let torrent = Torrent::new(name,
                    "",
                    files,
                    trackers,
                    piece_size,
                    None,
                    16384,
                    None,
                    comment,
                    Some("TestClient".to_string()),
                    tx).await;
        return Ok(torrent);
    }


    /*-- Verify & Hash ------------------------------------------------------*/

    async fn verify(&mut self, piece: usize) -> Option<()> {

        if let Ok(hash) = self.get_hash(piece) {
            if hash.eq(&self.pieces_hashes[piece]) {
                let mut info = self.info.lock().unwrap();
                info.is_piece_verified[piece] = true;

                let len = info.is_block_acquired[piece].len();
                info.is_block_acquired[piece] = vec![true; len];

                return match self.tx.try_send(ClientCommand::PieceVerify{ piece: piece }) {
                    Err(tokio::sync::mpsc::error::TrySendError::Full(_)) |
                    Ok(_) => Some(()),
                    Err(tokio::sync::mpsc::error::TrySendError::Closed(_)) => None,
                }
            }
            return None
        }

        let mut info = self.info.lock().unwrap();
        info.is_piece_verified[piece] = false;
        // All block are complete ?
        if info.is_block_acquired[piece].iter().all(|x| *x) {
            // Reset all
            let len = info.is_block_acquired[piece].len();
            info.is_block_acquired[piece] = vec![false; len];
        }

        None
    }

    pub fn get_hash(&self, piece: usize) -> Result<Vec<u8>> {
        let in_buffer = self.read_piece(piece)?;
        let mut sha1 = Sha1::new();
        sha1.input(&in_buffer);

        let mut out_buffer = vec![0;sha1.output_bytes()];
        sha1.result(&mut out_buffer);

        Ok(out_buffer)
    }

    /*-- Read & Write -------------------------------------------------------*/

    fn read_piece(&self, piece: usize) -> Result<Vec<u8>> {
        let mut start = 0;
        let mut len = 0;
        {
            let info = self.info.lock().unwrap();
            start = piece * info.piece_size;
            len = info.get_piece_size(piece);
        }

        self.read(start, len)
    }

    pub fn read_block(&self, piece: usize, offset: usize, len: usize)
        -> Result<Vec<u8>> {
        let mut start = 0;
        {
            let info = self.info.lock().unwrap();
            start = piece * info.piece_size + offset;
        }

        self.read(start, len)
    }

    fn read(&self, start: usize, len: usize) -> Result<Vec<u8>> {
        let end = start + len;
        let mut buffer = vec![0; len];

        if !Path::new(&self.download_directory).exists() {
            info!("Directory {} does not exist.", self.download_directory);
            return Err(Error::new(ErrorKind::NotFound, "Directory not found."));
        }

        for lock_file_item in &self.files {
            let file_item = &lock_file_item.read().unwrap();
            let offset = file_item.offset;
            let size = file_item.size;

            if (start < offset && end < offset)
                || (start > (offset + size) && end > (offset + size)) {
                continue;
            }

            let path_str = format!("{}/{}{}", &self.download_directory,
                &self.file_directory(),
                &file_item.path);

            if !Path::new(&path_str).exists() {
                if let Err(_) = File::create(&path_str) {
                    info!("Cannot create file \"{}\".", path_str);
                    return Err(Error::new(ErrorKind::NotFound, "File not found."));
                }
            }

            let f_start = cmp::max(0, start - offset);
            let f_end = cmp::min(end - offset, size);
            let f_len = f_end - f_start;
            let b_start = cmp::max(0, if offset > start { offset - start} else { 0 });

            let mut file = File::open(path_str)?;
            file.seek(SeekFrom::Start(f_start as u64))?;
            file.read(&mut buffer[b_start..(b_start + f_len)])?;
        }

        Ok(buffer)
    }

    fn write(&self, start: usize, bytes: Vec<u8>) -> Result<()> {
        info!("Torrent: write.");
        let end = start + bytes.len();

        for lock_file_item in &self.files {
            let file_item = &lock_file_item.write().unwrap();
            let offset = file_item.offset;
            let size = file_item.size;

            if (start < offset && end < offset)
                || (start > (offset + size) && end > (offset + size)) {
                continue;
            }

            let path_str = format!("{}/{}{}", &self.download_directory,
                &self.file_directory(),
                &file_item.path);

            let path = Path::new(&path_str);
            let dir = path.parent().unwrap();
            if !dir.exists() {
                fs::create_dir(dir).unwrap();
            };
            let mut file = fs::OpenOptions::new()
                .read(true)
                .write(true)
                .create(true)
                .open(path).unwrap();

            let f_start = if start > offset { start - offset } else { 0 };
            let f_end = cmp::min(end - offset, size);
            let f_len = f_end - f_start;
            let b_start = if offset > start { offset - start } else { 0 };

            file.write_all_at(&bytes[b_start..(b_start + f_len)], f_start as u64).unwrap();
        }

        info!("Torrent: write END.");
        Ok(())
    }

    pub async fn write_block(&mut self, piece: usize, block: usize, bytes: Vec<u8>)
        -> Result<()> {
        info!("Torrent: write block.");
        let mut start = 0;
        {
            let info = self.info.lock().unwrap();
            start = piece * info.piece_size + block * info.block_size;
        }
        self.write(start, bytes).unwrap();
        {
            self.info.lock().unwrap().is_block_acquired[piece][block] = true;
        }

        self.verify(piece).await;

        info!("Torrent: write block END.");
        Ok(())
    }

    /*-- Update Trackers ------------------------------------------------------*/

    pub async fn update_trackers(&mut self, event: TrackerEvent, id: &str, port: &str) {
        let hash = self.info_hash_str.clone();
        let uploaded = self.uploaded;
        let mut downloaded = 0;
        {
            downloaded = self.info.lock().unwrap().downloaded();
        }
        let left = self.left();

        let futures = self.trackers.iter_mut()
            .map(|x| x.update(&event, &hash, uploaded, downloaded, left, id, port));
        let sockets = future::join_all(futures).await
            .into_iter()
            .filter(|x| x.is_some())
            .flat_map(|x| x.unwrap())
            .collect::<Vec<SocketAddr>>();

        if let Err(_) = self.tx.send(ClientCommand::ListUpdate{ sockets: sockets }).await {
            info!("Torrent: update_trackers: send failed.");
        }
    }
}

#[cfg(test)]
mod test {
    use crate::torrent::{
        Torrent,
        FileItem,
    };
    use crate::command::ClientCommand;
    use crypto::digest::Digest;
    use crypto::sha1::Sha1;
    use tokio::sync::mpsc;
    use futures::executor::block_on;

    /*
    pub async fn new(name: &str,
               location: &str,
               files: Vec<FileItem>,
               trackers: Vec<String>,
               piece_size: usize,
               piece_hashes: Option<Vec<u8>>,
               block_size: usize,
               is_private: Option<bool>,
               comment: Option<String>,
               created_by: Option<String>,
               tx: Sender<ClientCommand>) -> Torrent {
    */

    fn get_hash(buffer: Vec<u8>) -> Vec<u8> {
        let mut sha1 = Sha1::new();
        sha1.input(&buffer);

        let len = sha1.output_bytes();
        let mut hash = vec![0; len];
        sha1.result(&mut hash);

        return hash;
    }

    #[test]
    fn test_write() {

        let block_size = 8 as usize;
        let piece_size = 8 as usize;
        let piece_num = 8 as usize;

        let mut piece_hashes = Vec::new();

        let mut pieces : Vec<Vec<Vec<u8>>> = Vec::new();
        for i in 0..piece_num {
            let mut piece : Vec<Vec<u8>> = Vec::new();
            for j in 0..piece_size {
                let mut block = vec!['0' as u8; block_size];
                block[0] = '0' as u8 + i as u8;
                block[1] = '0' as u8 + j as u8;
                block[7] = '#' as u8;
                piece.push(block);
            }

            let buffer = piece.clone().into_iter().flatten().collect::<Vec<u8>>();
            let mut hash = get_hash(buffer);
            piece_hashes.append(&mut hash);

            pieces.push(piece);
        }

        let f = FileItem {
            path: "test.txt.tmp".to_string(),
            size: block_size * piece_size * block_size,
            offset: 0,
        };

        let (tx, rx) = mpsc::channel::<ClientCommand>(1000);
        let mut torrent = block_on(Torrent::new(
            "test-torrent",
            "download_dir",
            vec![f],
            vec!["tracker-str".to_string()],
            piece_size * block_size,
            Some(piece_hashes),
            block_size,
            Some(false),
            None,
            Some("Test-function".to_string()),
            tx
        ));

        block_on(torrent.write_block(0, 0, pieces[0][0].clone()));
        block_on(torrent.write_block(7, 7, pieces[7][7].clone()));
        block_on(torrent.write_block(3, 3, pieces[3][3].clone()));
        block_on(torrent.write_block(3, 2, pieces[3][2].clone()));
        block_on(torrent.write_block(3, 4, pieces[3][4].clone()));
        /*
        for piece in piece_num..0 {
            for block in piece_size..0 {
                let p = pieces[piece][block].clone();
                block_on(torrent.write_block(piece, block, p));
            }
        }
        */
    }
}
