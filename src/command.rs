use std::{
    sync::{
        Arc,
        Mutex,
    },
    net::SocketAddr,
    time::Duration,
};

#[derive(Debug)]
pub enum ClientCommand {
    BlockCancel {
        id: String,
        piece: usize,
        begin: usize,
        length: usize,
    },
    BlockReceived {
        id: String,
        piece: usize,
        block: usize,
        data: Vec<u8>,
    },
    BlockRequest {
        id: String,
        index: usize,
        begin: usize,
        length: usize,
    },
    Disconnected {
        id: String,
    },
    ListUpdate {
        sockets: Vec<SocketAddr>,
    },
    PieceVerify {
        piece: usize,
    },
    StateChanged {
        id: String,
        pieces_downloaded: Option<Vec<usize>>,
    },
    SetSeeder {
        id: String,
    }
}

#[derive(Clone)]
pub enum PeerCommand {
    Disconnect,
    Process {
        is_completed: bool,
        is_started: bool,
        leechers_ok: Arc<Mutex<i8>>,
        seeders_ok: Arc<Mutex<i8>>,
        peer_timeout: Duration,
    },
    SendCancel {
        piece: usize,
        begin: usize,
        length: usize,
    },
    SendPiece {
        piece: usize,
        begin: usize,
        data: Vec<u8>,
    },
    SendRequest {
        index: usize,
        begin: usize,
        length: usize,
    },
    PieceVerified {
        index: usize,
    },
}
