use std::fs::File;
use std::iter::Peekable;
use std::io::{
    Bytes,
    Error,
    ErrorKind,
    Result,
};

pub trait PeekableBuffer {
    fn next(&mut self) -> Result<u8>;
    fn peek(&mut self) -> Result<u8>;
}

/*-- ByteBuffer --------------------------------------------------------------*/

pub struct ByteBuffer {
    bytes: Peekable<Bytes<File>>
}

impl ByteBuffer {
    pub fn new(b: Peekable<Bytes<File>>) -> ByteBuffer {
        ByteBuffer { bytes: b }
    }
}

impl PeekableBuffer for ByteBuffer {
    fn next(&mut self) -> Result<u8> {
        match self.bytes.next() {
            Some(Ok(val)) => Ok(val),
            _ => return Err(Error::new(ErrorKind::InvalidInput, "End Reached")),
        }
    }

    fn peek(&mut self) -> Result<u8> {
        match self.bytes.peek() {
            Some(Ok(val)) => Ok(*val),
            _ => return Err(Error::new(ErrorKind::InvalidInput, "End Reached")),
        }
    }
}

/*-- PeekableVec -------------------------------------------------------------*/

pub struct PeekableVec {
    bytes: Vec<u8>,
    offset: usize,
}

impl PeekableVec {
    pub fn new(v: Vec<u8>) -> PeekableVec {
        PeekableVec {
            bytes: v,
            offset: 0,
        }
    }
}

impl PeekableBuffer for PeekableVec {
    fn next(&mut self) -> Result<u8> {
        let val = self.peek();
        if val.is_ok() {
            self.offset += 1;
        }

        val
    }

    fn peek(&mut self) -> Result<u8> {
        if self.offset >= self.bytes.len() {
            return Err(Error::new(ErrorKind::InvalidInput, "End Reached"));
        }
        let val = self.bytes[self.offset];

        Ok(val)
    }
}

/*-- Tests -------------------------------------------------------------------*/

#[cfg(test)]
mod test {
    use crate::byte_buffer::{
        PeekableVec,
        PeekableBuffer,
    };

    #[test]
    fn test_bytebuffer_next() {
        assert!(false);
    }

    #[test]
    fn test_bytebuffer_peek() {
        assert!(false);
    }

    #[test]
    fn test_peekablevec_next() {
        let mut vec = PeekableVec::new(vec!(21, 42));
        assert_eq!(21, vec.next().unwrap());
        assert_eq!(42, vec.next().unwrap());
        assert!(match vec.next() { Err(_) => true, _ => false });
        assert!(match vec.next() { Err(_) => true, _ => false });
    }

    #[test]
    fn test_peekablevec_next_empty() {
        let mut vec = PeekableVec::new(vec!());
        assert!(match vec.next() { Err(_) => true, _ => false });
        assert!(match vec.next() { Err(_) => true, _ => false });
    }

    #[test]
    fn test_peekablevec_peek() {
        let mut vec = PeekableVec::new(vec!(21, 42));
        assert_eq!(21, vec.peek().unwrap());
        assert_eq!(21, vec.peek().unwrap());
    }

    #[test]
    fn test_peekablevec_peek_empty() {
        let mut vec = PeekableVec::new(vec!());
        assert!(match vec.peek() { Err(_) => true, _ => false });
    }
}
