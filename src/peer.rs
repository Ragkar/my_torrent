use crate::torrent::TorrentInfo;
use crate::command::{
    ClientCommand,
    PeerCommand,
};


use std::io::{
    Error,
    ErrorKind,
    Result,
};
use std::net::{
    SocketAddr,
};
use std::prelude::v1::*;
use std::time::{
    Instant,
    Duration,
};
use std::sync::{
    Arc,
    Mutex,
};
use std::str;

use tokio::{
    io::{
        AsyncWriteExt,
        AsyncReadExt,
    },
    time::timeout,
    net::{
        TcpStream,
    },
    sync::mpsc,
};

pub enum MessageType {
    Unknown     = -3,
    Handshake   = -2,
    KeepAlive   = -1,
    Choke       = 0,
    Unchoke     = 1,
    Interested  = 2,
    NotInterested = 3,
    Have        = 4,
    Bitfield    = 5,
    Request     = 6,
    Piece       = 7,
    Cancel      = 8,
    Port        = 9,
}

impl MessageType {
    pub fn is_defined(value: u8) -> bool {
        return value <= 9;
    }
}

impl From<u8> for MessageType {
    fn from(value: u8) -> Self {
        match value {
            0 => MessageType::Choke,
            1 => MessageType::Unchoke,
            2 => MessageType::Interested,
            3 => MessageType::NotInterested,
            4 => MessageType::Have,
            5 => MessageType::Bitfield,
            6 => MessageType::Request,
            7 => MessageType::Piece,
            8 => MessageType::Cancel,
            9 => MessageType::Port,
            _ => MessageType::Unknown
        }
    }
}

pub struct DataRequest {
    pub peer_id: String,
    pub piece: usize,
    pub begin: usize,
    pub length: usize,
    pub is_cancelled: bool,
}

pub struct DataPackage {
    pub peer_id: String,
    pub piece: usize,
    pub block: usize,
    pub data: Vec<u8>,
}

const BUFFER_SIZE: usize = 256;
const ID_SIZE: usize = 20;

const HANDSHAKE_SIZE: u8 = 68;
const HANDSHAKE_FIRST: u8 = 19;
const HANDSHAKE_PROTOCOLE: &str = "BitTorrent protocol";

pub struct Peer {
    // TODO: disconnected
    pub local_id: String,
    id: [u8; ID_SIZE],
    pub key: String,
    info: Arc<Mutex<TorrentInfo>>,
    tcp_stream: TcpStream,
    stream_buffer: [u8; BUFFER_SIZE],
    data: Vec<u8>,
    pub is_piece_downloaded: Vec<bool>,

    is_disconnected: bool,
    is_position_sent: bool,
    pub is_handshake_sent: bool,
    pub is_choke_sent: bool,
    pub is_interested_sent: bool,

    pub is_handshake_received: bool,
    pub is_choke_received: bool,
    pub is_interest_received: bool,

    pub last_active: Instant,
    last_keep_alive: Instant,

    uploaded: usize,
    downloaded: usize,

    tx: mpsc::Sender<ClientCommand>,
    rx: mpsc::Receiver<PeerCommand>,
}

pub enum Selected {
    Command { cmd: PeerCommand },
    TcpData,
    None,
}

impl Peer {

    /*-- Misc ---------------------------------------------------------------*/

    fn pieces_downloaded(&self) -> String {
        self.is_piece_downloaded.iter()
            .map(|&x| (x as u32).to_string())
            .collect::<Vec<String>>()
            .join("")
    }

    fn pieces_required_available(&self) -> usize {
        self.is_piece_downloaded.iter().enumerate()
            .map(|(i, &x)| (x && !self.info.lock().unwrap().is_piece_verified(i)) as usize)
            .sum()
    }

    fn pieces_downloaded_count(&self) -> usize {
        self.is_piece_downloaded.iter()
            .map(|&x| x as usize)
            .sum()
    }

    pub fn is_completed(&self) -> bool {
        self.pieces_downloaded_count() == self.info.lock().unwrap().pieces_count
    }

    /*-- Constructor --------------------------------------------------------*/

    pub async fn new(info: Arc<Mutex<TorrentInfo>>,
        local_id: String,
        socket: SocketAddr,
        tx: mpsc::Sender<ClientCommand>,
        rx: mpsc::Receiver<PeerCommand>) -> Option<Peer> {
        let pieces_count = info.lock().unwrap().pieces_count;
        info!("Sockets: {}.", socket.to_string());

        let fut_connect = TcpStream::connect(socket);
        let sleep = tokio::time::sleep(Duration::from_secs(5));
        tokio::pin!(sleep);

        tokio::select! {
            connect = fut_connect => {
                match connect {
                    Ok(tcp_stream) => {
                        info!("Sockets: {}, SUCCESS", socket.to_string());

                        Some(Peer {
                            local_id: local_id,
                            id: [0; ID_SIZE],
                            key: socket.to_string(),
                            info: info,
                            tcp_stream: tcp_stream,
                            stream_buffer: [0; BUFFER_SIZE],
                            data: Vec::new(),
                            is_piece_downloaded: vec![false; pieces_count],

                            is_disconnected: true,
                            is_handshake_sent: false,
                            is_position_sent: false,
                            is_choke_sent: false,
                            is_interested_sent: false,

                            is_handshake_received: false,
                            is_choke_received: false,
                            is_interest_received: false,

                            last_active: Instant::now(),
                            last_keep_alive: Instant::now(),

                            uploaded: 0,
                            downloaded: 0,

                            tx: tx,
                            rx: rx,
                        })
                    },
                    Err(e) => {
                        info!("Sockets: {}, FAILED", socket.to_string());
                        None
                    }
                }
            },
            _ = &mut sleep => {
                info!("Sockets: {}, TIMEOUT", socket.to_string());
                None
            }
        }
    }

    /*-- Connection ---------------------------------------------------------*/

    pub async fn connect(&mut self) -> Result<()> {
        if self.is_disconnected {
            info!("Peer: connected");

            // Send handshake
            self.send_handshake().await?;
            // TODO: why this ?
            if self.is_handshake_received {
                self.send_bitfield().await?;
            }

            self.is_disconnected = false;
        }

        Ok(())
    }

    pub async fn disconnect(&mut self, send: bool) -> Result<()> {
        if !self.is_disconnected {
            info!("disconnected");
            if send {
                if let Err(_) = self.tx.send(ClientCommand::Disconnected {
                    id: self.key.clone()
                }).await {
                    info!("Peer: disconnect: send failed.");
                }
            }
            self.is_disconnected = true;
        }

        Ok(())
    }

    /*-- I/O ----------------------------------------------------------------*/

    pub async fn run(&mut self) {
        let mut bytes = vec![0; 1024];
        while !self.is_disconnected {
            info!("Peer: run loop");
            let fut_cmd = self.rx.recv();
            let fut_tcp = self.tcp_stream.readable();

            let sleep = tokio::time::sleep(Duration::from_millis(500));
            tokio::pin!(sleep);

            tokio::select! {
                Some(command) = fut_cmd => {
                    info!("Peer({}): run: command.", self.key);
                    self.handle_command(command).await.unwrap();
                },
                //None = fut_cmd { },
                Ok(_) = fut_tcp => {
                    if let Ok(n) = self.tcp_stream.read(&mut bytes).await {
                        if n > 0 {
                            info!("Peer({}): run: tcp.", self.key);
                            self.handle_read(&mut bytes[0..n].to_vec()).await;
                        } else {
                            tokio::time::sleep(Duration::from_millis(500)).await;
                        }
                    } else {
                        info!("Peer({}): run: tcp failed.", self.key);
                    }
                },
                _ = &mut sleep => {
                    info!("Peer: wait timeout.");
                },
                else => {
                    info!("Peer({}): run: disconnected", self.key);
                    self.disconnect(true).await.unwrap();
                },
            }
        }
    }

    pub async fn recv_command(&mut self) {
        //info!("Peer({}): recv_command.", self.key);
        let duration = Duration::from_millis(500);
        while let ret = timeout(duration, self.rx.recv()).await {
            match ret {
                Ok(Some(cmd)) => self.handle_command(cmd).await.unwrap(),
                Ok(None) => break, //TODO
                Err(e) => break,
            }
        }
    }

    pub async fn recv_tcp(&mut self) {
        //info!("Peer({}): recv_tcp.", self.key);
        let mut bytes = vec![0; 1024];
        let duration = Duration::from_millis(500);
        while let ret = timeout(duration, self.tcp_stream.readable()).await {
            match ret {
                Ok(_) => {
                    match self.tcp_stream.try_read(&mut bytes) {
                        Ok(n) if n > 0 => {
                            //info!("Peer({}): run: read.", self.key);
                            self.handle_read(&mut bytes[0..n].to_vec()).await;
                        },
                        Ok(_) => break,
                        Err(e) if e.kind() == ErrorKind::WouldBlock => break,
                        Err(e) => {
                            error!("Peer({}): recv_tcp: {}.", self.key, e);
                            /*
                            self.disconnect(true);
                            */
                            break;
                        }
                    }
                },
                Err(e) => break,
            }
        }
    }

    pub async fn runner(&mut self) {
        if self.is_disconnected {
            return;
        }

        self.recv_command().await;
        self.recv_tcp().await;

        /*
        let fut_cmd = self.rx.recv();
        let fut_tcp = self.tcp_stream.readable();

        let sleep = tokio::time::sleep(Duration::from_millis(500));
        tokio::pin!(sleep);

        tokio::select! {
            Some(command) = fut_cmd => {
                info!("Peer({}): run: command.", self.key);
                self.handle_command(command).await.unwrap();
            },
            Ok(_) = fut_tcp => {
                if let Ok(n) = self.tcp_stream.read(&mut bytes).await {
                    if n > 0 {
                        info!("Peer({}): run: read.", self.key);
                        self.handle_read(&mut bytes[0..n].to_vec()).await;
                    }
                }
            },
            _ = &mut sleep => {
                info!("Peer: wait timeout.");
            },
        }
        */
    }

    async fn send_bytes(&mut self, bytes: Vec<u8>) -> Result<()> {
        let mut wait_writable = true;

        while wait_writable {
            let fut_tcp = self.tcp_stream.writable();
            let sleep = tokio::time::sleep(Duration::from_millis(500));
            tokio::pin!(sleep);

            tokio::select! {
                res = fut_tcp => { match res {
                        Ok(_) => wait_writable = false,
                        Err(_) => info!("Peer: err on writable !"),
                    }
                },
                _ = &mut sleep => {
                    info!("Peer: send_bytes: wait timeout.");
                },
            }
        }

        if let Err(_) = self.tcp_stream.write_all(&bytes).await {
            error!("send_bytes: tcp_stream.write_all fail.");
            self.disconnect(true).await?;
            //return Err(e);
        }

        Ok(())

    }

    pub async fn handle_read(&mut self, bytes: &mut Vec<u8>) {
        self.data.append(bytes);
        let mut len = Peer::get_message_length(self.is_handshake_received, &self.data);
        //info!("bytes.len({}), data.len({}), len({})", bytes.len(), self.data.len(), len);

        while self.data.len() >= len {
            let mut msg = self.data.split_off(len);
            std::mem::swap(&mut msg, &mut self.data);

            if let Err(e) = self.handle_message(msg).await {
                panic!("Peer: handle_read: {}", e);
            }
            len = Peer::get_message_length(self.is_handshake_received, &self.data);
        }
    }

    fn get_message_length(is_handshake_received: bool, data: &Vec<u8>) -> usize {
        if !is_handshake_received {
            return 68
        }
        if data.len() < 4 {
            return std::usize::MAX;
        }

        let mut arr: [u8; 4] = Default::default();
        arr.copy_from_slice(&data[0..4]);
        (u32::from_be_bytes(arr) + 4) as usize
    }

    /*-- Receiving ----------------------------------------------------------*/

    fn get_message_type(&self, bytes: &Vec<u8>) -> MessageType {
        if !self.is_handshake_received {
            return MessageType::Handshake;
        }

        let mut arr: [u8; 4] = Default::default();
        arr.copy_from_slice(&bytes[0..4]);
        let val = u32::from_be_bytes(arr);

        if bytes.len() == 4 && val == 0 {
            return MessageType::KeepAlive;
        }

        if bytes.len() > 4 && MessageType::is_defined(bytes[4]) {
            return MessageType::from(bytes[4]);
        }

        MessageType::Unknown
    }

    async fn handle_message(&mut self, bytes: Vec<u8>) -> Result<()> {
        self.last_active = Instant::now();
        let mtype = self.get_message_type(&bytes);
        match mtype {
            MessageType::Unknown => {
                info!("received unknown...");
                Ok(())
            },
            MessageType::Handshake =>  match Peer::decode_handshake(bytes) {
                Ok((hash, id)) => self.handle_handshake(hash, id).await,
                Err(e) => Err(e)
            },
            MessageType::KeepAlive => match Peer::decode_keepalive(bytes) {
                Ok(()) => self.handle_keepalive(),
                Err(e) => Err(e),
            },
            MessageType::Choke => match Peer::decode_choke(bytes) {
                Ok(()) => self.handle_choke().await,
                Err(e) => Err(e),
            },
            MessageType::Unchoke => match Peer::decode_unchoke(bytes) {
                Ok(()) => self.handle_unchoke().await,
                Err(e) => Err(e),
            },
            MessageType::Interested => match Peer::decode_interested(bytes) {
                Ok(()) => self.handle_interested().await,
                Err(e) => Err(e),
            },
            MessageType::NotInterested => match Peer::decode_not_interested(bytes) {
                Ok(()) => self.handle_not_interested().await,
                Err(e) => Err(e),
            },
            MessageType::Have => match Peer::decode_have(bytes) {
                Ok(index) => self.handle_have(index).await,
                Err(e) => Err(e),
            },
            MessageType::Bitfield => match Peer::decode_bitfield(bytes, self.is_piece_downloaded.len()) {
                Ok(is_piece_downloaded) => self.handle_bitfield(is_piece_downloaded).await,
                Err(e) => Err(e)
            },
            MessageType::Request => match Peer::decode_request(bytes) {
                Ok((index, begin, length)) => self.handle_request(index, begin, length).await,
                Err(e) => Err(e),
            },
            MessageType::Piece => match Peer::decode_piece(bytes) {
                Ok((index, begin, data)) => self.handle_piece(index, begin, data).await,
                Err(e) => Err(e),
            },
            MessageType::Cancel => match Peer::decode_cancel(bytes) {
                Ok((index, begin, length)) => self.handle_cancel(index, begin, length).await,
                Err(e) => Err(e),
            },
            MessageType::Port => {
                info!("<-port");
                Ok(())
            },
        }
    }

    /*-- Encoding & Decoding ------------------------------------------------*/

    /*-- Handshake --*/
    fn decode_handshake(bytes: Vec<u8>) -> Result<(Vec<u8>, Vec<u8>)> {
        info!("decode_handshake.");
        if bytes.len() != (HANDSHAKE_SIZE as usize) || bytes[0] != HANDSHAKE_FIRST {
            info!("Invalid handshake: bad size or bad first byte.");
            return Err(Error::new(ErrorKind::InvalidData, "Invalid handshake"));
        }

        if str::from_utf8(&bytes[1..20]) != Ok(HANDSHAKE_PROTOCOLE) {
            info!("Invalid handshake: bad protocol.");
            return Err(Error::new(ErrorKind::InvalidData, "Invalid handshake"));
        }

        let mut hash = vec![0; 20];
        hash.copy_from_slice(&bytes[28..48]);

        let id = bytes[48..68].to_vec();

        Ok((hash, id))
    }

    fn encode_handshake(hash: &Vec<u8>, id: &mut str) -> Vec<u8> {
        let mut msg = Vec::new();

        msg.push(HANDSHAKE_FIRST);
        msg.append(&mut HANDSHAKE_PROTOCOLE.clone().as_bytes().to_vec());
        msg.append(&mut vec![0;8]);
        msg.append(&mut hash[0..20].to_vec());
        msg.append(&mut id.as_bytes()[0..20].to_vec());

        msg
    }

    async fn send_handshake(&mut self) -> Result<()> {
        if !self.is_handshake_sent {
            info!("Peer: send handshake");

            let hash = &self.info.lock().unwrap().info_hash.clone();
            let mut id = self.local_id.clone();
            let bytes = Peer::encode_handshake(hash, &mut id);
            self.send_bytes(bytes).await?;

            self.is_handshake_sent = true;
        }

        Ok(())
    }

    async fn handle_handshake(&mut self, hash: Vec<u8>, id: Vec<u8>) -> Result<()> {
        info!("Peer: handle handshake");
        if hash != self.info.lock().unwrap().info_hash {
            error!("Invalid handshake: {:?} {:?}", hash, self.info.lock().unwrap().info_hash);
            self.disconnect(true).await?;
            return Err(Error::new(ErrorKind::InvalidData, format!("Invalid handshake")));
        }

        self.id.copy_from_slice(&id[0..ID_SIZE]);
        self.is_handshake_received = true;
        self.send_bitfield().await
    }

    /*-- KeepAlive --*/
    fn decode_keepalive(bytes: Vec<u8>) -> Result<()> {
        info!("decode_keepalive.");

        let mut arr: [u8; 4] = Default::default();
        arr.copy_from_slice(&bytes[0..4]);
        let val = u32::from_be_bytes(arr);

        if bytes.len() != 4 && val != 0 {
            return Err(Error::new(ErrorKind::InvalidData, "Invalid keepalive"));
        }

        Ok(())
    }

    fn encode_keepalive() -> Vec<u8> {
        let val: u32 = 0;
        val.to_be_bytes().to_vec()
    }

    pub async fn send_keepalive(&mut self) -> Result<()> {
        let elapse = Instant::now().duration_since(self.last_keep_alive);
        if elapse > Duration::from_secs(30) {
            info!("Peer: send KeepAlive");
            let bytes = Peer::encode_keepalive();
            self.send_bytes(bytes).await?;
            self.last_keep_alive = Instant::now();
        }

        Ok(())
    }

    fn handle_keepalive(&self) -> Result<()> {
        info!("Peer: handle KeepAlive");
        Ok(())
    }

    /*-- State --*/
    fn decode_state(bytes: Vec<u8>, mtype: MessageType) -> Result<()> {
        info!("decode_state.");
        let mut arr: [u8; 4] = Default::default();
        arr.copy_from_slice(&bytes[0..4]);
        let val = u32::from_be_bytes(arr);
        let type_u8 = mtype as u8;

        if bytes.len() != 5 || val != 1 || bytes[4] != type_u8 {
            let val = format!("Invalid state of type {}({}).", bytes[4], type_u8);
            return Err(Error::new(ErrorKind::InvalidData, val));
        }

        Ok(())
    }

    fn encode_state(mtype: MessageType) -> Vec<u8> {
        let val: u32 = 1;
        let mut vec = val.to_be_bytes().to_vec();
        vec.push(mtype as u8);

        vec
    }

    /*-- Choke --*/
    fn decode_choke(bytes: Vec<u8>) -> Result<()> {
        info!("decode_choke.");
        Peer::decode_state(bytes, MessageType::Choke)
    }

    fn encode_choke() -> Vec<u8> {
        Peer::encode_state(MessageType::Choke)
    }

    async fn send_choke(&mut self) -> Result<()> {
        if !self.is_choke_sent {
            info!("Peer: send choke");
            let bytes = Peer::encode_choke();
            self.send_bytes(bytes).await?;
            self.is_choke_sent = true;
        }

        Ok(())
    }

    async fn handle_choke(&mut self) -> Result<()> {
        info!("Peer: handle Choke");
        self.is_choke_received = true;

        if let Err(_) = self.tx.send(ClientCommand::StateChanged {
            id: self.key.clone(),
            pieces_downloaded: None,
        }).await {
            info!("Peer: handle_choke: send failed.");
        }
        Ok(())
    }

    /*-- Unchoke --*/
    fn decode_unchoke(bytes: Vec<u8>) -> Result<()> {
        info!("Peer: decode_unchoke.");
        Peer::decode_state(bytes, MessageType::Unchoke)
    }

    fn encode_unchoke() -> Vec<u8> {
        Peer::encode_state(MessageType::Unchoke)
    }

    pub async fn send_unchoke(&mut self) -> Result<()> {
        if self.is_choke_sent {
            info!("Peer: send Unchoke");
            let bytes = Peer::encode_unchoke();
            self.send_bytes(bytes).await?;
            self.is_choke_sent = false;
        }

        Ok(())
    }

    async fn handle_unchoke(&mut self) -> Result<()> {
        info!("Peer: handle Unchoke");
        self.is_choke_received = false;

        if let Err(_) = self.tx.send(ClientCommand::StateChanged {
            id: self.key.clone(),
            pieces_downloaded: None,
        }).await {
            info!("Peer: handle_unchoke: send failed.");
        }
        Ok(())
    }

    /*-- Interested --*/
    fn decode_interested(bytes: Vec<u8>) -> Result<()> {
        info!("decode_interested.");
        Peer::decode_state(bytes, MessageType::Interested)
    }

    fn encode_interested() -> Vec<u8> {
        Peer::encode_state(MessageType::Interested)
    }

    pub async fn send_interested(&mut self) -> Result<()> {
        if !self.is_interested_sent {
            info!("Peer: send Interested");
            let bytes = Peer::encode_interested();
            self.send_bytes(bytes).await?;
            self.is_interested_sent = true;
        }

        Ok(())
    }

    async fn handle_interested(&mut self) -> Result<()> {
        info!("Peer: handle Interested");
        self.is_interest_received = true;

        if let Err(_) = self.tx.send(ClientCommand::StateChanged {
            id: self.key.clone(),
            pieces_downloaded: None,
        }).await {
            info!("Peer: handle_interested: send failed.");
        }
        Ok(())
    }

    /*-- NotInterested --*/
    fn decode_not_interested(bytes: Vec<u8>) -> Result<()> {
        info!("decode_not_interested.");
        Peer::decode_state(bytes, MessageType::NotInterested)
    }

    fn encode_not_interested() -> Vec<u8> {
        Peer::encode_state(MessageType::NotInterested)
    }

    pub async fn send_not_interested(&mut self) -> Result<()> {
        if self.is_interested_sent {
            info!("Peer: send NotInterested");
            let bytes = Peer::encode_not_interested();
            self.send_bytes(bytes).await?;
            self.is_interested_sent = false;
        }

        Ok(())
    }

    async fn handle_not_interested(&mut self) -> Result<()> {
        info!("Peer: handle NotInterested");
        self.is_interest_received = false;

        if let Err(_) = self.tx.send(ClientCommand::StateChanged{
            id: self.key.clone(),
            pieces_downloaded: None,
        }).await {
            info!("Peer: handle_not_interested: send failed.");
        }
        Ok(())
    }

    /*-- Have --*/
    fn decode_have(bytes: Vec<u8>) -> Result<usize> {
        info!("decode_have.");
        let mut arr: [u8; 4] = Default::default();
        arr.copy_from_slice(&bytes[0..4]);
        let val = u32::from_be_bytes(arr);

        if bytes.len() != 9 || val != 5 {
            return Err(Error::new(ErrorKind::InvalidData, "Invalid have"));
        }

        arr.copy_from_slice(&bytes[5..9]);
        Ok(u32::from_be_bytes(arr) as usize)
    }

    fn encode_have(index: usize) -> Vec<u8> {
        let val: u32 = 5;

        let mut msg = Vec::with_capacity(9);
        msg.append(&mut val.to_be_bytes().to_vec());
        msg.push(MessageType::Have as u8);
        msg.append(&mut (index as u32).to_be_bytes().to_vec());

        msg
    }

    pub async fn send_have(&mut self, index: usize) -> Result<()> {
        info!("Peer: send Have {}", index);
        let bytes = Peer::encode_have(index);
        self.send_bytes(bytes).await
    }

    async fn handle_have(&mut self, index: usize) -> Result<()> {
        info!("Peer: handle Have {}", index);
        self.is_piece_downloaded[index] = true;

        if let Err(_) = self.tx.send(ClientCommand::StateChanged{
            id: self.key.clone(),
            pieces_downloaded: Some(vec![index]),
        }).await {
            info!("Peer: handle_have: send failed.");
        }
        info!("Peer: handle Have end.");
        Ok(())
    }

    /*-- Bitfield
     *
     * The bitfield message is a quick way to communicate the peer's progress
     * and is usually sent directly after the handshake. It is a little tricker.
     * After the type byte, every bit specifies whether or not the peer has
     * downloaded the piece at that index.
     */
    fn decode_bitfield(bytes: Vec<u8>, pieces: usize) -> Result<Vec<bool>> {
        info!("decode_bitfield.");
        let expected_len = (((pieces as f64) / 8.0).ceil()) as usize + 1;

        let mut arr: [u8; 4] = Default::default();
        arr.copy_from_slice(&bytes[0..4]);
        let val = u32::from_be_bytes(arr) as usize;

        if bytes.len() != expected_len + 4 || val != expected_len {
            return Err(Error::new(ErrorKind::InvalidData, "Invalid bitfield"));
        }

        let mut bits: Vec<bool> = Vec::new();
        for i in bytes[5..].iter() {
            for shift in (0..8).rev() {
                bits.push(((i >> shift) as u8 & (1 as u8)) == 1);
            }
        }
        Ok(bits[0..pieces].to_vec())
    }

    fn encode_bitfield(is_piece_downloaded: &Vec<bool>) -> Vec<u8> {
        let num_pieces = is_piece_downloaded.len();
        let num_bytes = (num_pieces as f64 / 8.0).ceil() as usize;
        let num_bits = num_bytes * 8;
        let len = num_bytes + 1;

        let mut msg = Vec::with_capacity(len + 4);
        msg.append(&mut (len as u32).to_be_bytes().to_vec());
        msg.push(MessageType::Bitfield as u8);

        let mut bits = Vec::new();
        let mut u = 0;
        for i in 0..num_bits {
            let boolean = if i < len { is_piece_downloaded[i] } else { false };
            let b = if boolean { 1 } else { 0 };
            u = (u << 1) + b;

            if (i + 1) % 8 == 0 {
                bits.push(u);
                u = 0;
            }
        }
        msg.append(&mut bits);

        msg
    }

    async fn send_bitfield(&mut self) -> Result<()> {
        info!("Peer: send Bitfield");
        let bytes = Peer::encode_bitfield(&self.info.lock().unwrap().is_piece_verified);
        self.send_bytes(bytes).await
    }

    async fn handle_bitfield(&mut self, is_piece_downloaded: Vec<bool>) -> Result<()> {
        let mut pieces_downloaded = Vec::new();
        for i in 0..self.info.lock().unwrap().pieces_count {
            if !self.is_piece_downloaded[i] && is_piece_downloaded[i] {
                self.is_piece_downloaded[i] |= is_piece_downloaded[i];
                pieces_downloaded.push(i);
            }
        }
        info!("Peer: handle Bitfield ({}/{}).", pieces_downloaded.len(), self.is_piece_downloaded.len());

        if let Err(_) = self.tx.send(ClientCommand::StateChanged{
            id: self.key.clone(),
            pieces_downloaded: Some(pieces_downloaded),
        }).await {
            info!("Peer: handle_bitfield: send failed.");
        }
        Ok(())
    }

    /*-- Request --*/
    fn decode_request(bytes: Vec<u8>) -> Result<(usize, usize, usize)> {
        info!("decode_request.");
        let mut arr: [u8; 4] = Default::default();
        arr.copy_from_slice(&bytes[0..4]);

        let length = u32::from_be_bytes(arr) as usize;
        if bytes.len() != 17 && length != 13 {
            return Err(Error::new(ErrorKind::InvalidData, "Invalid request"))
        }

        arr.copy_from_slice(&bytes[5..9]);
        let index = u32::from_be_bytes(arr) as usize;

        arr.copy_from_slice(&bytes[9..13]);
        let begin = u32::from_be_bytes(arr) as usize;

        arr.copy_from_slice(&bytes[13..17]);
        let length = u32::from_be_bytes(arr) as usize;

        Ok((index, begin, length))
    }

    fn encode_request(index: usize, begin: usize, length: usize) -> Vec<u8> {
        let val: u32 = 13;

        let mut msg = Vec::with_capacity(17);
        msg.append(&mut val.to_be_bytes().to_vec());
        msg.push(MessageType::Request as u8);
        msg.append(&mut (index as u32).to_be_bytes().to_vec());
        msg.append(&mut (begin as u32).to_be_bytes().to_vec());
        msg.append(&mut (length as u32).to_be_bytes().to_vec());

        msg
    }

    pub async fn send_request(&mut self, index: usize, begin: usize, length: usize) -> Result<()> {
        info!("Peer: send request {}, {}, {}", index, begin, length);
        let bytes = Peer::encode_request(index, begin, length);
        self.send_bytes(bytes).await
    }

    async fn handle_request(&self, index: usize, begin: usize, length: usize) -> Result<()> {
        info!("Peer: handle Request {}, {}, {}", index, begin, length);
        if let Err(_) = self.tx.send(ClientCommand::BlockRequest {
            id: self.key.clone(),
            index,
            begin,
            length,
        }).await {
            info!("Peer: handle_request: send failed.");
        }
        Ok(())
    }

    /*-- Piece --*/
    fn decode_piece(bytes: Vec<u8>) -> Result<(usize, usize, Vec<u8>)> {
        info!("decode_piece.");
        if bytes.len() < 13 {
            // TODO: Fail here
            return Err(Error::new(ErrorKind::InvalidData, "Invalid piece"))
        }

        let mut arr: [u8; 4] = Default::default();

        arr.copy_from_slice(&bytes[0..4]);
        let length = (u32::from_be_bytes(arr) - 9) as usize;

        arr.copy_from_slice(&bytes[5..9]);
        let index = u32::from_be_bytes(arr) as usize;

        arr.copy_from_slice(&bytes[9..13]);
        let begin = u32::from_be_bytes(arr) as usize;

        let mut data = Vec::with_capacity(length);
        let mut mbytes = bytes[13..(13 + length)].to_vec();
        data.append(&mut mbytes);

        Ok((index, begin, data))
    }

    fn encode_piece(index: usize, begin: usize, data: Vec<u8>) -> Vec<u8> {
        let length = data.len() + 9;
        let mut mdata = data;

        let mut msg = Vec::with_capacity(length + 4);
        msg.append(&mut (length as u32).to_be_bytes().to_vec());
        msg.push(MessageType::Piece as u8);
        msg.append(&mut (index as u32).to_be_bytes().to_vec());
        msg.append(&mut (begin as u32).to_be_bytes().to_vec());
        msg.append(&mut mdata);

        msg
    }

    pub async fn send_piece(&mut self, index: usize, begin: usize, data: Vec<u8>) -> Result<()> {
        info!("Peer: send piece {}, {}", index, begin);
        let bytes = Peer::encode_piece(index, begin, data);
        self.send_bytes(bytes).await
    }

    async fn handle_piece(&mut self, index: usize, begin: usize, data: Vec<u8>) -> Result<()> {
        info!("Peer: receive Piece {}, {}", index, begin);
        let block = begin / self.info.lock().unwrap().block_size;
        info!("Piece({}): index({}), begin({}), data.len({})\n{:?}..{:?}",
            self.key, index, begin, data.len(),
            str::from_utf8(&data[0..2]),
            str::from_utf8(&data[10..20]));

        self.downloaded += data.len();

        if let Err(_) = self.tx.send(ClientCommand::BlockReceived {
            id: self.key.clone(),
            piece: index,
            block: block,
            data: data,
        }).await {
            info!("Peer: handle_piece: Send failed.");
        }
        Ok(())
    }

    /*-- Cancel --*/
    fn decode_cancel(bytes: Vec<u8>) -> Result<(usize, usize, usize)> {
        info!("decode_cancel.");
        let mut arr: [u8; 4] = Default::default();
        arr.copy_from_slice(&bytes[0..4]);

        let length = u32::from_be_bytes(arr) as usize;
        if bytes.len() != 17 && length != 13 {
            return Err(Error::new(ErrorKind::InvalidData, "Invalid piece"))
        }

        arr.copy_from_slice(&bytes[5..9]);
        let index = u32::from_be_bytes(arr) as usize;

        arr.copy_from_slice(&bytes[9..13]);
        let begin = u32::from_be_bytes(arr) as usize;

        arr.copy_from_slice(&bytes[13..17]);
        let length = u32::from_be_bytes(arr) as usize;

        Ok((index, begin, length))
    }

    fn encode_cancel(index: usize, begin: usize, length: usize) -> Vec<u8> {
        let val: u32 = 13;

        let mut msg = Vec::with_capacity(17);
        msg.append(&mut val.to_be_bytes().to_vec());
        msg.push(MessageType::Cancel as u8);
        msg.append(&mut (index as u32).to_be_bytes().to_vec());
        msg.append(&mut (begin as u32).to_be_bytes().to_vec());
        msg.append(&mut (length as u32).to_be_bytes().to_vec());

        msg
    }

    pub async fn send_cancel(&mut self, index: usize, begin: usize, length: usize) -> Result<()> {
        info!("Peer: send cancel {}, {}, {}", index, begin, length);
        let bytes = Peer::encode_cancel(index, begin, length);
        self.send_bytes(bytes).await
    }

    async fn handle_cancel(&self, index: usize, begin: usize, length: usize) -> Result<()> {
        info!("Peer: handle Cancel {}, {}, {}", index, begin, length);
        if let Err(_) = self.tx.send(ClientCommand::BlockCancel{
            id: self.key.clone(),
            piece: index,
            begin: begin,
            length: length,
        }).await {
            info!("Peer: handle_cancel: Send failed.");
        }
        Ok(())
    }

    /*-- PeerCommand ---------------------------------------------------------*/

    async fn handle_command(&mut self, command: PeerCommand) -> Result<()> {
        info!("Peer: handle command.");
        match command {
            PeerCommand::Disconnect => {
                info!("handle_command: disconnect.");
                self.disconnect(false).await.unwrap();
            },
            PeerCommand::PieceVerified { index: p } => self.piece_verified(p).await,
            PeerCommand::Process {
                is_completed,
                is_started,
                leechers_ok,
                seeders_ok,
                peer_timeout
            } => self.process(is_completed, is_started, leechers_ok, seeders_ok, peer_timeout).await,
            PeerCommand::SendCancel {
                piece,
                begin,
                length,
            } => self.send_cancel(piece, begin, length).await.unwrap(),
            PeerCommand::SendPiece {
                piece,
                begin,
                data
            } => self.send_piece(piece, begin, data).await.unwrap(),
            PeerCommand::SendRequest {
                index,
                begin,
                length,
            } => self.send_request(index, begin, length).await.unwrap(),
        }

        Ok(())
    }

    async fn piece_verified(&mut self, index: usize) {
        if self.is_handshake_received && self.is_handshake_sent {
            self.send_have(index).await.unwrap();
        }
    }

    async fn process(&mut self,
            is_completed: bool,
            is_started: bool,
            leechers: Arc<Mutex<i8>>,
            seeders: Arc<Mutex<i8>>,
            peer_timeout: Duration) {
        info!("Peer: process.");

        let duration = Instant::now().duration_since(self.last_active);
        if duration > peer_timeout {
            info!("Peer: process: timeout (skip).");
            //self.disconnect(true).await.unwrap();
        }

        if !self.is_handshake_received || !self.is_handshake_sent {
            info!("Peer: process: No handshake !");
            return ;
        }

        // Check completed possible with channel
        if is_completed {
            self.send_not_interested().await.unwrap();
        } else {
            self.send_interested().await.unwrap();
        }

        // Completed with channel
        if self.is_completed() && is_completed {
            self.disconnect(true).await.unwrap();
            return;
        }

        self.send_keepalive().await.unwrap();
        {
            let l = *leechers.lock().unwrap();
            if is_started && l > 0 {
                if self.is_interest_received && self.is_choke_sent {
                    self.send_unchoke().await.unwrap();
                }
            }
        }
        {
            let mut s = *seeders.lock().unwrap();
            if !is_completed && s > 0 {
                if !self.is_choke_received {
                    s -= 1;
                    // TODO! fail here (already exist)
                    // How to handle had of some seeder already existing ?
                    if let Err(_) = self.tx.try_send(ClientCommand::SetSeeder{ id: self.key.clone() }) {
                        info!("Peer: process: Send failed.");
                    }
                }
            }
        }
    }
}
