use crate::byte_buffer::{
    PeekableBuffer,
    ByteBuffer,
};

use std::collections::{
    HashMap,
    BTreeMap,
};
use std::io::{
    Error,
    ErrorKind,
    Read,
    Result,
};
use std::fmt;
use std::fs::File;

#[derive(Debug)]
pub enum BEObject {
    Int  { val: u64, },
    List { val: Vec<BEObject> },
    Dict { val: HashMap<String, BEObject> },
    Str  { val: Vec<u8> },
}

impl BEObject {
    const INT_DELIM: u8 = ('i' as u8);
    const DICT_DELIM: u8 = ('d' as u8);
    const LIST_DELIM: u8 = ('l' as u8);
    const END_DELIM: u8 = ('e' as u8);
    const SPLIT_DELIM: u8 = (':' as u8);

    pub fn from_file(path: &str) -> Result<BEObject> {
        info!("BEObject -> from_file.");
        let f = File::open(path)?;
        let mut bytes = ByteBuffer::new(f.bytes().peekable());
        let ret = BEObject::from_buffer(&mut bytes);
        info!("BEObject <- from_file.");
        ret
    }

    /*-----------------------------------------------------------------------*\
     * Decode
    \*-----------------------------------------------------------------------*/

    pub fn from_buffer(bytes: &mut dyn PeekableBuffer) -> Result<BEObject> {
        match bytes.peek()? {
            BEObject::INT_DELIM => BEObject::read_int(bytes),
            BEObject::LIST_DELIM => BEObject::read_list(bytes),
            BEObject::DICT_DELIM => BEObject::read_dict(bytes),
            val => {
                BEObject::read_str(bytes)
            },
        }
    }

    fn read_int(bytes: &mut dyn PeekableBuffer) -> Result<BEObject> {
        // Skip the begin deliminter
        bytes.next()?;

        let mut e = bytes.next()?;
        let mut i: u64 = 0;

        while e != BEObject::END_DELIM {
            i = i * 10 + ((e as char).to_digit(10).unwrap() as u64);
            e = bytes.next()?;
        };
        // End delimiter already consume

        Ok(BEObject::Int { val: i })
    }

    fn read_str(bytes: &mut dyn PeekableBuffer) -> Result<BEObject> {
        let mut e = bytes.next()?;
        let mut len: usize = 0;
        let mut s = Vec::new();

        // Read the length
        while e != BEObject::SPLIT_DELIM {
            len = len * 10 + ((e as char).to_digit(10).unwrap() as usize);
            e = bytes.next()?;
        }
        // Read the string
        for _ in 0..len {
            e = bytes.next()?;
            s.push(e);
        };

        Ok(BEObject::Str { val: s })
    }

    fn read_list(bytes: &mut dyn PeekableBuffer) -> Result<BEObject> {
        info!("BEObject -> read_list.");
        // Skip the begin deliminter
        bytes.next()?;

        let mut e = bytes.peek()?;
        let mut l = Vec::new();

        while e != BEObject::END_DELIM {
            l.push(BEObject::from_buffer(bytes)?);
            e = bytes.peek()?;
        };
        // Consume the end delimiter
        bytes.next()?;
        info!("BEObject <- read_list.");

        Ok(BEObject::List { val: l })
    }

    fn read_dict(bytes: &mut dyn PeekableBuffer) -> Result<BEObject> {
        info!("BEObject -> read_dict.");
        // Skip the begin deliminter
        bytes.next()?;

        let mut e = bytes.peek()?;
        let mut d = HashMap::new();

        while e != BEObject::END_DELIM {
            // Read key and value
            let k = BEObject::from_buffer(bytes)?;
            let v = BEObject::from_buffer(bytes)?;
            e = bytes.peek()?;

            // Check that the key validity
            let temp_v = match k {
                BEObject::Str { val: s } => s,
                _ => return Err(Error::new(ErrorKind::InvalidInput, "Key string is not in UTF8")),
            };
            let ks = match String::from_utf8(temp_v) {
                Ok(s) => s,
                Err(_) => return Err(Error::new(ErrorKind::InvalidInput, "Key string is not in UTF8")),
            };

            d.insert(ks, v);
        }
        // Consume the end delimiter
        bytes.next()?;
        info!("BEObject <- read_dict.");

        Ok(BEObject::Dict { val: d })
    }

    /*-----------------------------------------------------------------------*\
     * Encode
    \*-----------------------------------------------------------------------*/

    pub fn to_buffer(&self) -> Result<Vec<u8>> {
        let mut vec = Vec::new();
        match self.add_to_buffer(&mut vec) {
            Ok(_) => Ok(vec),
            Err(e) => Err(e),
        }
    }

    fn add_to_buffer(&self, bytes: &mut Vec<u8>) -> Result<()> {
        match self {
            BEObject::Int { val: i } => BEObject::write_int(i, bytes),
            BEObject::Str { val: s } => BEObject::write_str(s, bytes),
            BEObject::List { val: l } => BEObject::write_list(l, bytes),
            BEObject::Dict { val: d } => BEObject::write_dict(d, bytes),
        }
    }

    fn write_int(i: &u64, bytes: &mut Vec<u8>) -> Result<()> {
        let mut s = i.to_string().into_bytes();
        bytes.push(BEObject::INT_DELIM);
        bytes.append(&mut s);
        bytes.push(BEObject::END_DELIM);
        Ok(())
    }

    fn write_str(s: &Vec<u8>, bytes: &mut Vec<u8>) -> Result<()> {
        let mut v = s.clone();
        let mut len = s.len().to_string().into_bytes();
        bytes.append(&mut len);
        bytes.push(BEObject::SPLIT_DELIM);
        bytes.append(&mut v);
        Ok(())
    }

    fn write_list(l: &Vec<BEObject>, bytes: &mut Vec<u8>) -> Result<()> {
        bytes.push(BEObject::LIST_DELIM);
        for o in l {
            o.add_to_buffer(bytes)?;
        }
        bytes.push(BEObject::END_DELIM);
        Ok(())
    }

    fn write_dict(data: &HashMap<String, BEObject>, bytes: &mut Vec<u8>) -> Result<()> {
        bytes.push(BEObject::DICT_DELIM);
        // Need to be sort (standard)
        let ordered = data.iter()
            .map(|(k, v)| (k.as_bytes().to_vec(), v))
            .collect::<BTreeMap<Vec<u8>, &BEObject>>();
        for (k, v) in ordered.iter() {
            BEObject::write_str(&k, bytes)?;
            v.add_to_buffer(bytes)?;
        }
        bytes.push(BEObject::END_DELIM);
        Ok(())
    }
}

impl std::fmt::Display for BEObject {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            BEObject::Int { val: _ } => write!(f, "BEObject::Int()"),
            BEObject::Str { val: _ } => write!(f, "BEObject::Str()"),
            BEObject::List { val: _ } => write!(f, "BEObject::List()"),
            BEObject::Dict { val: _ } => write!(f, "BEObject::Dict()"),
        }
    }
}

#[cfg(test)]
mod test {
    use crate::be_encoding::BEObject;
    use crate::byte_buffer::{
        PeekableVec,
    };

    #[test]
    fn test_read_int() {
        let mut vec = PeekableVec::new(vec!(BEObject::INT_DELIM, '2' as u8, '1' as u8, BEObject::END_DELIM, '0' as u8));
        let res = BEObject::read_int(&mut vec);
        assert_eq!(21, match res { Ok(BEObject::Int { val: i}) => i, _ => 0 });
    }

    #[test]
    fn test_read_str() {
        let mut vec = PeekableVec::new(vec!('3' as u8, BEObject::SPLIT_DELIM, 's' as u8, 't' as u8, 'r' as u8, '0' as u8));
        let res = BEObject::read_str(&mut vec);
        assert_eq!(vec!['s' as u8, 't' as u8, 'r' as u8], match res { Ok(BEObject::Str { val: v }) => v, _ => vec![] });
    }

    #[test]
    fn test_read_list() {
        let mut vec = PeekableVec::new(vec!(
            BEObject::LIST_DELIM,
            BEObject::LIST_DELIM, BEObject::END_DELIM,
            BEObject::LIST_DELIM, BEObject::END_DELIM,
            BEObject::END_DELIM
        ));
        if let BEObject::List { val: res } = BEObject::read_list(&mut vec).unwrap() {
            assert!(match &res[0] { BEObject::List { val: v } => v.len() == 0, _ => false });
            assert!(match &res[1] { BEObject::List { val: v } => v.len() == 0, _ => false });
        } else {
            assert!(false);
        }
    }

    #[test]
    fn test_read_dict() {
        let mut vec = PeekableVec::new(vec!(
            BEObject::DICT_DELIM,
            '3' as u8, BEObject::SPLIT_DELIM, 's' as u8, 't' as u8, 'r' as u8,
            BEObject::DICT_DELIM,
            '1' as u8, BEObject::SPLIT_DELIM, 's' as u8,
            '1' as u8, BEObject::SPLIT_DELIM, 't' as u8,
            BEObject::END_DELIM,
            BEObject::END_DELIM
        ));
        if let BEObject::Dict { val: mut res } = BEObject::read_dict(&mut vec).unwrap() {
            if let Some(BEObject::Dict { val: mut v1 }) = res.remove("str") {
                assert_eq!(vec!['t' as u8], match v1.remove("s") { Some(BEObject::Str { val: v }) => v, _ => vec![]});
            } else {
                assert!(false);
            }
        } else {
            assert!(false);
        }
    }
}
