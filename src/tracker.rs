use crate::be_encoding::BEObject;
use crate::byte_buffer::PeekableVec;

use hyper::{
    Body,
    Client,
    Response,
    Uri,
};
use hyper::body::{
    to_bytes,
};
use tokio::time::{
    Duration,
    Instant,
};

use std::fmt;
use std::net::{
    IpAddr,
    Ipv4Addr,
    SocketAddr
};
use std::ops::Sub;
use std::u16;

type HttpClient = Client<hyper::client::HttpConnector, Body>;
type Event = Fn(&Tracker, Vec<Ipv4Addr>) -> ();

#[derive(Debug, PartialEq)]
pub enum TrackerEvent {
    Started,
    Paused,
    Stopped,
}

impl fmt::Display for TrackerEvent {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

pub struct Tracker {
    // TODO: peer_list_update
    pub address: String,
    last_peer_request: Instant,
    peer_request_interval: Duration,
    http_client: HttpClient,
}

impl Tracker {

    pub fn new(url: &str) -> Tracker {
        Tracker {
            address: url.to_string(),
            last_peer_request: Instant::now().sub(Duration::from_secs(30 * 60)),
            peer_request_interval: Duration::from_secs(30 * 60),
            http_client: Client::new(),
        }
    }

    /*-- Update -------------------------------------------------------------*/

    pub async fn update(&mut self, event: &TrackerEvent,
                  hash: &str,
                  uploaded: usize,
                  downloaded: usize,
                  left: usize,
                  id: &str, port: &str) -> Option<Vec<SocketAddr>> {
        let elapse = Instant::now().duration_since(self.last_peer_request);
        if TrackerEvent::Started == *event && elapse < self.peer_request_interval {
            //sleep(Duration::from_secs(30 * 50)).await;
            return None;
        }

        self.last_peer_request = Instant::now();
        let url = format!("{}?info_hash={}&peer_id={}&port={}&uploaded={}&downloaded={}&left={}&event={}&compact=1",
            self.address,
            hash,
            id,
            port,
            uploaded,
            downloaded,
            left,
            event.to_string());
        let uri: Uri = url.parse().unwrap();
        return self.request(uri).await;
    }

    async fn request(&mut self, uri: Uri) -> Option<Vec<SocketAddr>> {
        let response = self.http_client.get(uri).await.unwrap();
        return self.handle_response(response).await;
    }

    async fn handle_response(&mut self, response: Response<Body>) -> Option<Vec<SocketAddr>> {
        if !response.status().is_success() {
            info!("Error reaching tracker {}: {}", self.address, response.status().as_str());
            return None;
        }

        let data = match to_bytes(response.into_body()).await {
            Ok(chunk) => chunk.as_ref().to_vec(),
            Err(e) => {
                print!("Error getting data from {}: {}", self.address, e);
                return None;
            },
        };
        let mut buffer = PeekableVec::new(data);

        let mut info = match BEObject::from_buffer(&mut buffer) {
            Ok(BEObject::Dict { val: vd }) => vd,
            _ => {
                info!("Error parsing info from {}", self.address);
                return None;
            },
        };

        match info.remove("failure reason") {
            Some(BEObject::Str { val: vs }) => info!("{}", String::from_utf8(vs).unwrap()),
            _ => {},
        };

        self.peer_request_interval = match info.remove("interval") {
            Some(BEObject::Int { val: vi }) => Duration::from_secs(vi),
            _ => {
                info!("Error getting intervals from {}", self.address);
                return None;
            }
        };

        let peer_info = match info.remove("peers") {
            Some(BEObject::Str { val: vs}) => vs,
            _ => {
                info!("Error getting peers from {}", self.address);
                return None;
            },
        };

        let mut sockets = Vec::new();
        for i in 0..peer_info.len() / 6 {
            let j = i * 6;

            let mut s_ip: [u8; 4] = Default::default();
            s_ip.copy_from_slice(&peer_info[j .. j + 4]);
            let ip = Ipv4Addr::from(s_ip);

            let mut s_port: [u8; 2] = Default::default();
            s_port.copy_from_slice(&peer_info[j + 4 .. j + 6]);
            let port = u16::from_be_bytes(s_port);

            sockets.push(SocketAddr::new(IpAddr::V4(ip) , port));
        }

        return Some(sockets);
    }
}
