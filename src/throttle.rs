use std::time::{
    Duration,
    Instant,
};

pub struct Throttle {
    time_limit: Duration,
    last_check: Instant,
    bytes_limit: usize,
    bytes: usize,
}

impl Throttle {
    pub fn new(time_limit: Duration, bytes_limit: usize) -> Throttle {
        Throttle {
            time_limit: time_limit,
            last_check: Instant::now(),
            bytes_limit: bytes_limit,
            bytes: 0,
        }
    }

    pub fn add_bytes(&mut self, bytes: usize) {
        self.bytes += bytes;
    }

    pub async fn check_and_wait(&mut self) {
        let duration = Instant::now().duration_since(self.last_check);
        if duration > self.time_limit {
            if self.bytes > self.bytes_limit {
                let bytes_overflow_rate = (self.bytes - self.bytes_limit) as f32
                    / (self.bytes_limit as f32);

                let time_overflow_rate = (duration - self.time_limit)
                    .div_f32(self.time_limit.as_secs_f32())
                    .as_secs_f32();

                let overflow_rate = bytes_overflow_rate - time_overflow_rate;
                if overflow_rate > 0.0 {
                    info!("Throttle: check_and_wait ({})", overflow_rate);
                    tokio::time::sleep(Duration::from_secs_f32(overflow_rate)).await;
                }
            }
            self.bytes = 0;
            self.last_check = Instant::now();
        }
    }
}
