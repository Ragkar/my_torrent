mod be_encoding;
mod byte_buffer;
mod torrent;
mod tracker;
mod peer;
mod client;
mod throttle;
mod command;

extern crate crypto;
extern crate futures;
extern crate get_local_ip;
extern crate hyper;
#[macro_use]
extern crate log;
extern crate std;
extern crate tokio;
extern crate urlencoding;
extern crate env_logger;

use client::Client;
use log::*;
use std::env;
use std::io::prelude::*;

//use client::Client;


#[tokio::main(flavor = "multi_thread", worker_threads = 2)]
async fn main() {
    let start = std::time::Instant::now();
    env_logger::Builder::from_default_env().format(move |buf, rec| {
        let t = start.elapsed().as_secs_f32();
        writeln!(buf, "{:.03} [{}] - {}", t, rec.level(),rec.args())
    }).init();

    let args: Vec<String> = env::args().collect();
    if args.len() == 4 {
        let port: usize = args[1].parse().unwrap();
        let mut client = Client::new(port, args[2].clone(), args[3].clone()).await.unwrap();
        client.start().await;

        // tokio::signal::ctrl_c().await.unwrap();
    } else {
        info!("usage: {} <PORT> <TORRENT_PATH> <DOWNLOAD_DIR>", args[0]);
    }
}
